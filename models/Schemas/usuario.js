const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create geolocation Schema
const GeoSchema = new Schema({
    type: {
        type: String,
        default: 'Point'
    },
    coordinates: {
        type: [Number],
        index: '2dsphere'
    }
});

// create usuario Schema & model
const UsuarioSchema = new Schema({
    name: {
        type: String
    },
    surname: {
        type: String
    },
    email: {
        type: String,
        required: [true, 'Email field is required']
    },
    pass: {
      type: String,
      required: [true, 'Pass field is required']
    },
    role: {
        type: String
    },
    rank: {
        type: Number,
        default: 1
    },
    available: {
        type: Boolean,
        default: false
    },
    geometry: GeoSchema,
    extraResources: {
        type: Array
    },
    tag: {
        type : String,
        default: ''
    }
});

const Usuario = mongoose.model('usuario', UsuarioSchema);

module.exports = Usuario;
