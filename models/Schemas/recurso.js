const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create geolocation Schema
const GeoSchema = new Schema({
    type: {
        type: String,
        default: 'Point'
    },
    coordinates: {
        type: [Number],
        index: '2dsphere'
    }
});

//console.log('este es el geoschema de recurso /t', GeoSchema);

// create usuario Schema & model
const RecursoSchema = new Schema({
    ip_plug: {
        type: String
    },
    name: {
        type: String,
        required: [true, 'Name field is required']
    },
    description: {
        type: String
    },
    rank: {
        type: Number,
        default: 1,
        required: [true, 'rank field is required']
    },
    available: {
        type: Boolean,
        default: false
    },
    geometry: GeoSchema
});

const Recurso = mongoose.model('recurso', RecursoSchema);

module.exports = Recurso;
