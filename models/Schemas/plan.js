const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// create time Schema & model
var dateSchema = new Schema({
    startTime: Date,
    endTime: Date,
}, {
    timestamps: true
});


// create planning Schema & model
const PlanningSchema = new Schema({
    date: dateSchema,
    resource_id: {
    	type: String
    },
    user_id: {
    	type: String
    }
});

const Planning = mongoose.model('planning', PlanningSchema);

module.exports = Planning;
