var VistaLogin = function(id){
	this.inputEmail = '#email';
	this.inputPass = '#password';
	this.btnLogin = '#btnLogin';
	this.classDesactivateClick = '.divContenedor';
	this.btnLoginFacebook = '#btnLoginFacebook';
	this.id = $(id);
}
VistaLogin.prototype = {
	constructor: VistaLogin,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-login-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},
	loadEvents: function(){
		//this.eventInputs();
		this.eventButtons();

	},
	eventInputs: function(){
		var t= this;
		$(t.password+','+t.email).keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if ((code==13)){
				$(t.btnLogin).click();
			}
			return true;
		});
	},
	eventButtons: function(){
		var t = this;
		$(t.btnLogin).on('click', function(){
			$(t.classDesactivateClick).addClass('desactivarClick');
			var email, pass;
			email = $(t.inputEmail).val();
			pass = $(t.inputPass).val();


			//aqui comprobamos la conexión e iniciamos en un modo de conexión u otro

			if($(t.inputEmail).val() != '' && $(t.inputPass).val() != ''){

				var u = new ModeloUser('','','',email,'','','','','',pass,'');
				u.getForCredenciales().then(respuesta => {
					var userLogin = respuesta[0];



					if (userLogin != undefined){
						var pass_decrypted = CryptoJS.AES.decrypt(userLogin.pass, "@1e").toString(CryptoJS.enc.Utf8);
						if(pass_decrypted == pass){
							console.log("el usuario tiene el role de : "+userLogin.role);
							if (userLogin.role == 'ADMIN'){
								$(t.classDesactivateClick).addClass('desactivarClick');

								$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
								var v = new VistaHomeAdmin('#divL'+contPages, userLogin);

								contPages++;
								t.moveOut();
								v.loadcss('../css/canvasCSS/offcanvas.css');
								v.loadHTML('../html/vista-HomeAdmin.html').then(function(){
									v.loadContent().then(function(){
										v.loadEvents().then(function(){
											$('#user_recursos').click();
										});
										v.fadeInView();
									});

								});

							}else{
								$(t.classDesactivateClick).addClass('desactivarClick');

								$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
								var v = new VistaHome('#divL'+contPages, userLogin);

								contPages++;
								t.moveOut();
								v.loadcss('../css/canvasCSS/offcanvas.css');
								v.loadHTML('../html/vista-Home.html').then(function(){
									v.loadContent().then(function(){
									v.loadEvents().then(function(){
											$('#btn_eventos_calendario').click();
									});
									v.fadeInView();
									});
								});
							}
						}else{
							$(t.classDesactivateClick).removeClass('desactivarClick');
							alert("El usuario o contraseña es incorrecta.",'','', "Cerrar");
						}

					}else {
						$(t.classDesactivateClick).removeClass('desactivarClick');
						alert("El usuario no existe",'','', "Cerrar");
					}
				});

			}else{
				$(t.classDesactivateClick).removeClass('desactivarClick');
				alert("Rellene todos los campos correctamente.",'','', "Cerrar");
			}
		});


		$('#btnRegistrarse').on('click', function(){

			$(t.classDesactivateClick).addClass('desactivarClick');

			$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
			var v = new VistaRegistro('#divL'+contPages);

			contPages++;
			t.moveOut();
			v.loadcss('../css/login.css');
					v.loadHTML('../html/vista-Registro.html').then(function(){
						v.loadEvents();
						v.fadeInView();

					});

		});


	},

};
