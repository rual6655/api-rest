var VistaRegistro = function(id){
	this.inputEmail = '#email';
	this.inputPass1 = '#password1';
	this.inputPass2 = '#password2';
	this.btnRegistro = '#btnRegistro';
	this.classDesactivateClick = '.divContenedor';
	this.id = $(id);
}
VistaRegistro.prototype = {
	constructor: VistaRegistro,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-registro-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},
	loadEvents: function(){
		//this.eventInputs();
		this.eventButtons();

	},
	eventInputs: function(){
		var t= this;
		$(t.password+','+t.email).keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if ((code==13)){
				$(t.btnRegistro).click();
			}
			return true;
		});
	},
	eventButtons: function(){
		var t = this;
		$(t.btnRegistro).on('click', function(){
			$(t.classDesactivateClick).addClass('desactivarClick');
			var email, pass1, pass2;
			email = $(t.inputEmail).val();
			pass1 = $(t.inputPass1).val();
			pass2 = $(t.inputPass2).val();


			//aqui comprobamos la conexión e iniciamos en un modo de conexión u otro

			if($(t.inputEmail).val() != '' && $(t.inputPass1).val() != '' && $(t.inputPass2).val() != '' ){
				if ($(t.inputPass1).val() == $(t.inputPass2).val()){

					var pass_encrypted = CryptoJS.AES.encrypt(pass1, "@1e");

					var u = new ModeloUser('','','',email,'','','','','',pass_encrypted.toString(),'');
					u.getForCredenciales().then(respuesta => {
						console.log("hay "+respuesta.length+" con ese email");
						if (respuesta.length == 0 ){
							var newUser= {"email": email, "password" : pass_encrypted.toString()};
							console.log("este es el nuevo usuario para registrar ---- "+JSON.stringify(newUser));
							u.addUser().then(respuesta => {
								console.log("usuario registrado ----- "+JSON.stringify(respuesta));
								if (respuesta != undefined ){
									$(t.classDesactivateClick).addClass('desactivarClick');

									$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
									var v = new VistaLogin('#divL'+contPages);

									contPages++;
									t.moveOut();
									v.loadcss('../css/login.css');
							        v.loadHTML('html/vista-Login.html').then(function(){
												v.loadEvents();
												v.fadeInView();

											});

								}else {
									$(t.classDesactivateClick).removeClass('desactivarClick');
									alert("Error al registrar",'','', "Cerrar");
								}
							});

						}else {
							$(t.classDesactivateClick).removeClass('desactivarClick');
							alert("Existe un usuario con ese email",'','', "Cerrar");
						}
					});
				}else{
					$(t.classDesactivateClick).removeClass('desactivarClick');
					alert("Las contraseñas no coinciden.",'','', "Cerrar");
				}


			}else{
				$(t.classDesactivateClick).removeClass('desactivarClick');
				alert("Rellene todos los campos correctamente.",'','', "Cerrar");
			}
		});


		$('#btnVolver').on('click', function(){

			$(t.classDesactivateClick).addClass('desactivarClick');

			$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
			var v = new VistaLogin('#divL'+contPages);
				contPages++;
				t.moveOut();
				v.loadcss('../css/login.css');
		        v.loadHTML('../html/vista-Login.html').then(function(){
							v.loadEvents();
							v.fadeInView();

						});
		});


	},

};
