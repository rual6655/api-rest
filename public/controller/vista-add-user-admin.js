var VistaPerfilAddUser = function(id){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.userSession = '';
	this.btnGuardarPerfil ='#btnGuardarPerfil';
	this.btnResetPerfil = '#btnResetPerfil';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.role_selected = 0;
	this.rank_selected = 1;
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/checked.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha añadido al usuario</h6><small></small></div></div>';

}
VistaPerfilAddUser.prototype = {
	constructor: VistaPerfilAddUser,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-perfil-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("perfil del usuario: "+JSON.stringify(t.userSession));
		t.loadContent();
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},

	loadEvents: function(){
		//this.eventInputs();
		this.eventButtons();


	},

	loadContent: function (){
		var t = this;

		console.log($('#divMain'));

	},

	eventButtons: function(){
		var t = this;
		$(t.btnGuardarPerfil).off().on('click', function(){

			$(t.classDesactivateClick).addClass('desactivarClick');

			var selected_role;
			var selected_rank;
			if ($('#select_role').val() == 3){
				selected_role = "ADMIN";
			}else if ($('#select_role').val() == 2){
				selected_role ="PROFESOR"
			}else if ($('#select_role').val() == 1){
				selected_role ="ESTUDIANTE"
			}else{
				selected_role = "";
			}
			if ($('#select_rank').val() == 3){
				selected_rank = 3;
			}else if ($('#select_rank').val() == 2){
				selected_rank = 2;
			}else if ($('#select_rank').val() == 1){
				selected_rank = 1;
			}else{
				selected_rank = 1;
			}

			//aqui comprobamos la conexión e iniciamos en un modo de conexión u otro

			if($('#email_input').val() != '' && $('#password_input').val() != '' ){


					var pass_encrypted = CryptoJS.AES.encrypt($('#password_input').val(), "@1e");

					var  newUser= {
						"name":  $('#nombre_input').val(),
						"surname": $('#apellido_input').val(),
						"email": $('#email_input').val(),
						"pass": pass_encrypted.toString(),
						"role": selected_role,
						"rank": selected_rank,
						"available": "",
						"geometry": "",
						"extraResources": ""
					};
					console.log("usuario registrado ----- ", newUser);
					var u = new ModeloUser('',newUser.name,newUser.surname,newUser.email,newUser.role,newUser.rank,'','',newUser.extraResources,pass_encrypted.toString(),'');
					u.getForCredenciales().then(respuesta => {
						console.log("hay "+respuesta.length+" con ese email");
						if (respuesta.length == 0 ){
							console.log("este es el nuevo usuario para registrar ---- "+JSON.stringify(newUser));
							u.addUser().then(respuesta => {
								console.log("usuario registrado ----- "+JSON.stringify(respuesta));
								if (respuesta != undefined ){
									$(t.classDesactivateClick).addClass('desactivarClick');

									$('#btn_dropdown_users').click();
									//$('#btn_close_modal').click();
									$('#modal_grid_continer').append(t.alert_green_perfil);

								}else {
									$(t.classDesactivateClick).removeClass('desactivarClick');
									alert("Error al registrar",'','', "Cerrar");
								}
							});

						}else {
							$(t.classDesactivateClick).removeClass('desactivarClick');
							alert("Existe un usuario con ese email",'','', "Cerrar");
						}
					});



			}else{
				$(t.classDesactivateClick).removeClass('desactivarClick');
				alert("Rellene todos los campos correctamente.",'','', "Cerrar");
			}

		});

		$(t.btnResetPerfil).on('click', function(){
			$.xPromptQ({header: '¿Estas seguro resetear a este usuario?'}, function(i){
		    console.log(i)
				if (i==true){
					$('#btn_close_modal').click();
				}
		  });

		});


	},


};
