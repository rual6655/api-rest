var VistaGeo = function(id){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
}
VistaGeo.prototype = {
	constructor: VistaGeo,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('vista geo->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
	    var fileref=document.createElement("link");
	    fileref.setAttribute("rel", "stylesheet");
	    fileref.setAttribute("type", "text/css");
	    fileref.setAttribute("href", css_file);
	    document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
	},
	moveOut: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	loadEvents: function(){
		if ("geolocation" in navigator){ //check geolocation available
		        //try to get user current location using getCurrentPosition() method
		        navigator.geolocation.getCurrentPosition(function(position){
		        console.log("Found your location Lat : "+position.coords.latitude+" Lang :"+ position.coords.longitude);
		        /*$("#result").html("Found your location Lat : "+position.coords.latitude+" Lang :"+ position.coords.longitude);

		        $("#latitude").val(position.coords.latitude);
		        $("#longitude").val(position.coords.longitude);*/
		    });
		}else{
		   console.log("Browser doesn't support geolocation!");
		}


	},
};
