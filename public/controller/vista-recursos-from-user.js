var VistaGesResourcesFromUser = function(id, sessionDashboard){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.sessionDashboard = sessionDashboard;
	this.userSession = sessionDashboard.userSession;
	this.list_resources = "";
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha actulizado la información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.modal_grid_continer = '<!-- Modal --><div class="modal fade bd-example-modal-lg" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h5 id="header_modal_text" class="modal-title" id="exampleModalLongTitle">Modal title</h5><button id="btn_close_modal" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="modal_grid_continer" class="container-fluid"></div></div></div></div></div>'
}
VistaGesResourcesFromUser.prototype = {
	constructor: VistaGesResourcesFromUser,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-recursos-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log($('#divMain'));
		$('body').append(t.modal_grid_continer);
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},

	loadContent: function(respuesta){
		var t= this;
		t.list_resources=respuesta;
		return new Promise(function(resolve, reject) {

			$('#contenedor_cards').append('<div id="vista_uso_resource" class="card"><div class="card-header" style=""><div class="table-responsive"><table id="table_uso_recurso" class="table table-sm"><thead><tr><th scope="col">#</th><th scope="col">IP_PLUG</th><th scope="col">NOMBRE</th><th scope="col">RANGO</th><th scope="col">DISPONIBILIDAD</th><th scope="col">DESCRIPCIÓN</th></tr></thead><tbody></tbody></table></div></div>');

			console.log('el numero de recursos cargados: '+t.list_resources.length);
			for(i=0; i<respuesta.length; i++){

				if (t.list_resources[i].rank == 1){
					var rank_resource= 'Público';
				}else if (t.list_resources[i].rank == 3){
					var rank_resource= 'Especial';

				}else{
					var rank_resource= 'Limitado';

				}

				if(t.list_resources[i].available == true){

					$('#table_uso_recurso > tbody').append('<tr><th scope="row">'+i+'</th><td>'+t.list_resources[i].ip_plug+'</td><td>'+t.list_resources[i].name+'</td><td>'+rank_resource+'</td><td><img style="width: 20px; display:block; margin-top: 2px; margin-left: 55px;" src="../img/true.svg"></img></td><td>'+t.list_resources[i].description+'</td></tr>');

				}else{
					$('#table_uso_recurso > tbody').append('<tr><th scope="row">'+i+'</th><td>'+t.list_resources[i].ip_plug+'</td><td>'+t.list_resources[i].name+'</td><td>'+rank_resource+'</td><td><img style="width: 20px; display:block; margin-top: 2px; margin-left: 55px;" src="../img/false.svg"></img></td><td>'+t.list_resources[i].description+'</td></tr>');

				}
				//t.eventButtons(i);

			}

			resolve();
	});
	},

	eventButtons: function(i){
		var t = this;

		$('#mostrar_users').off().on('click', function(){
				$('.card_contenedor').css('display', 'none');
				console.log('mostrar por:', $('#filtro_ges_user').val());
			if($('#filtro_ges_user').val() == 3){
				$('.1').css('display', '');
			}else if($('#filtro_ges_user').val() == 4){
				$('.2').css('display', '')
			}else if($('#filtro_ges_user').val() == 5){
				$('.3').css('display', '')
			}else if($('#filtro_ges_user').val() == 'Choose...'){
				$('.card_contenedor').css('display', '')
			}
		});


		$('#btnModificarUser_'+i+'').off().on('click', function(){
			console.log(this.value);
			console.log('El recurso a modificar: ',t.list_resources[this.value]);
			$('#modal_grid_continer').empty();

			$('#header_modal_text').text('Modificar recurso');

			$('#modal_grid_continer').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaGesResource('#divL'+contPages, t.list_resources[this.value]);
			contPages++;
			v.loadHTML('../html/vista-ges-recursos.html').then(function(){
				v.loadContent().then(function(){
					v.loadEvents();
					v.fadeInView();
				});
			});

		});

		$('#btnVerHistorial_'+i+'').off().on('click', function(){
			console.log(this.value);
			console.log('Mostrar uso de recurso: ',t.list_resources[this.value]);
			$('#modal_grid_continer').empty();

			$('#header_modal_text').text('Historial recurso');

			$('#modal_grid_continer').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaUseResource('#divL'+contPages, t.list_resources[this.value]);
			contPages++;
			var u = new ModeloResource(t.list_resources[this.value]._id,t.list_resources[this.value].ip_plug,t.list_resources[this.value].name,t.list_resources[this.value].description,t.list_resources[this.value].rank,t.list_resources[this.value].available,t.list_resources[this.value].geometry);
			u.getHistorial().then(respuesta => {
				v.loadHTML('../html/vista-uso-recurso.html').then(function(){
					v.loadContent(respuesta).then(function(){
						v.fadeInView();
					});
				});
			});

		});

		$('#add_user_admin_perfil').off().on('click', function(){

			$('#modal_grid_continer').empty();

			$('#header_modal_text').text('Añadir recurso');

			$('#modal_grid_continer').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaPerfilAddResource('#divL'+contPages, '');
			contPages++;
			v.loadHTML('../html/vista-ges-recursos.html').then(function(){
					v.loadContent();
					v.loadEvents();
					v.fadeInView();
				});

		});
		/*$('#btnModificarUser_'+i+'').off().on('click', function(){
			console.log(this.value());
		});*/
		$('#btnEliminarUser_'+i+'').off().on('click', function(){
			console.log(this.value);
			console.log('El usuario a eliminar',t.list_resources[this.value]);
			var id_user_delete=t.list_resources[this.value]._id;


		  $.xPromptQ({header: '¿Estas seguro de eliminar a este usuario?'}, function(i){
		    console.log(i)
				if (i==true){
					var u = new ModeloResource(id_user_delete,'','','','','','','','','','');
					u.delete().then(respuesta => {
						$('#btn_dropdown_resources').click();
					});
				}
		  });
		});

	},

};
