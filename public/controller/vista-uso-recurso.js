var VistaUseResource = function(id, sessionUserList, desde){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.userSession = sessionUserList;
	this.btnGuardarPerfil ='#btnGuardarPerfil';
	this.btnResetPerfil = '#btnResetPerfil';
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+sessionUserList.name+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionUserList.name+'</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha actulizado la información para el recurso</h6><small>Recurso: '+sessionUserList.name+'</small></div></div>';
	this.role_selected = 0;
	this.rank_selected = 1;
	this.list_uses =[];
	this.desde=desde;

}
VistaUseResource.prototype = {
	constructor: VistaUseResource,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-uso_recurso-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("uso del usuario : "+JSON.stringify(t.userSession));
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},
	loadEvents: function(){
		this.eventButtons();
	},

	loadContent: function (respuesta){
		var t= this;
		t.list_uses=respuesta;
		return new Promise(function(resolve, reject) {
			console.log('el número de usos cargados: '+t.list_uses.length);
			console.log('desde: '+t.desde);
			if (t.desde == 'ges_recursos'){
				for(var i=0; i<respuesta.length; i++){

					$('#table_uso_recurso > tbody').append('<tr><th scope="row">'+i+'</th><td>'+t.list_uses[i]._id+'</td><td>'+new Date(t.list_uses[i].date.startTime).toUTCString()+'</td><td>'+new Date(t.list_uses[i].date.endTime).toUTCString()+'</td><td>'+t.list_uses[i].resource_id+'</td><td id="id_usuario_'+i+'" style="color: blue; cursor: pointer; text-decoration-line: underline;" data-toggle="modal" data-target="#exampleModalLong2">'+t.list_uses[i].user_id+'</td></tr>');
					t.eventButtonsRecursos(i);

				}
			}else if (t.desde == 'ges_usuarios'){
				for(var i=0; i<respuesta.length; i++){

					$('#table_uso_recurso > tbody').append('<tr><th scope="row">'+i+'</th><td>'+t.list_uses[i]._id+'</td><td>'+new Date(t.list_uses[i].date.startTime).toUTCString()+'</td><td>'+new Date(t.list_uses[i].date.endTime).toUTCString()+'</td><td id="id_recurso_'+i+'" style="color: blue; cursor: pointer; text-decoration-line: underline;" data-toggle="modal" data-target="#exampleModalLong2">'+t.list_uses[i].resource_id+'</td><td>'+t.list_uses[i].user_id+'</td></tr>');
					t.eventButtonsUsuarios(i);

				}
			}else if (t.desde == 'ges_usuarios_admin'){
				for(var i=0; i<respuesta.length; i++){

					$('#table_uso_recurso > tbody').append('<tr><th scope="row">'+i+'</th><td>'+t.list_uses[i]._id+'</td><td>'+new Date(t.list_uses[i].date.startTime).toUTCString()+'</td><td>'+new Date(t.list_uses[i].date.endTime).toUTCString()+'</td><td id="id_recurso_'+i+'" style="color: blue; cursor: pointer; text-decoration-line: underline;" data-toggle="modal" data-target="#exampleModalLong2">'+t.list_uses[i].resource_id+'</td><td>'+t.list_uses[i].user_id+'</td><td><button id="btnEliminarUser_'+i+'" value='+i+' type="reset" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i>Eliminar</button></td></tr>');
					t.eventButtonsUsuarios(i);
					t.eventButtonAdmin(i);

				}
			}else if (t.desde == 'user'){
				for(var i=0; i<respuesta.length; i++){

					$('#table_uso_recurso > tbody').append('<tr><th scope="row">'+i+'</th><td>'+t.list_uses[i]._id+'</td><td>'+new Date(t.list_uses[i].date.startTime).toUTCString()+'</td><td>'+new Date(t.list_uses[i].date.endTime).toUTCString()+'</td><td id="id_recurso_'+i+'" style="color: blue; cursor: pointer; text-decoration-line: underline;" data-toggle="modal" data-target="#exampleModalLong2">'+t.list_uses[i].resource_id+'</td><td>'+t.list_uses[i].user_id+'</td><td><button id="btnEliminarUser_'+i+'" value='+i+' type="reset" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i>Eliminar</button></td></tr>');
					t.eventButtonsUsuarios(i);
					t.eventButton(i);

				}
			}


			resolve();
	});
	},
	eventButton: function(i){
		var t = this;
		$('#btnEliminarUser_'+i).off().on('click', function(){
			console.log(this.value);
			console.log(t.list_uses[this.value]._id);
			var u = new ModeloPlan(t.list_uses[this.value]._id,'','','','');

			var time_plan_to_delete = new Date(new Date(date_reloj).toUTCString()) - new Date (t.list_uses[this.value].date.startTime);
			console.log(time_plan_to_delete);
			if (time_plan_to_delete >= 0){
				alert('Las reservas comenzadas no se pueden eliminar','','', "Cerrar");
			}else{
				$.xPromptQ({header: '¿Estas seguro de eliminar este plan?'}, function(i){
				 console.log(i)
				 if (i==true){
					 u.deleteBefore().then(respuesta => {
						 $('#user_reservas').click();
						 console.log('respuesta delete reserva: ---- ', respuesta);
					 });
				 }
			 });
			}
		});
	},

	eventButtonAdmin: function(i){
		var t = this;
		$('#btnEliminarUser_'+i).off().on('click', function(){
			console.log(this.value);
			console.log(t.list_uses[this.value]._id);
			var u = new ModeloPlan(t.list_uses[this.value]._id,'','','','');

			var time_plan_to_delete = new Date(new Date(date_reloj).toUTCString()) - new Date (t.list_uses[this.value].date.startTime);
			console.log(time_plan_to_delete);
			if (time_plan_to_delete >= 0){
				alert('Las reservas comenzadas no se pueden eliminar','','', "Cerrar");
			}else{
				$.xPromptQ({header: '¿Estas seguro de eliminar este plan?'}, function(i){
				 console.log(i)
				 if (i==true){
					 u.deleteBefore().then(respuesta => {
						 $('#user_reservas').click();
						 console.log('respuesta delete reserva: ---- ', respuesta);
					 });
				 }
			 });
			}
		});
	},
	eventButtonsRecursos: function(i){
		var t = this;

			$('#id_usuario_'+i).off().on('click', function(){
				//alert($(this).text());

				$('#modal_grid_continer2').empty();

				$('#header_modal_text2').text('Perfil');

				$('#modal_grid_continer2').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');

				var u = new ModeloUser($(this).text(),'','','','','','','','','','');
				u.getForID().then(respuesta => {
					console.log('respuesta get for id user: ---- ', respuesta);

						var v = new VistaPerfilGesUser('#divL'+contPages, respuesta);
						contPages++;
						v.loadHTML('../html/vista-perfil_admin.html').then(function(){
							v.loadContentExtraResources().then(function(){
							v.loadContent().then(function(){
								if (t.userSession.role != 'ADMIN'){
									$('#nombre_input').prop("disabled", true);
									$('#apellido_input').prop("disabled", true);
									$('#password_input').prop("disabled", true);
									$('#select_role').prop("disabled", true);
									$('#select_rank').prop("disabled", true);
									$('#multiple_select').prop("disabled", true);
									$('#btnGuardarPerfil').prop("disabled", true);
									$('#btnResetPerfil').prop("disabled", true);
								}
								v.loadEvents();
								v.fadeInView();
							});
						});
						});


			}).catch(e => {
    		alert("El usuario ya no se encuentra registrado en ActiveSpace",'','', "Cerrar");
			});


			});

	},
	eventButtonsUsuarios: function(i){
		var t = this;

			$('#id_recurso_'+i).off().on('click', function(){
				//alert($(this).text());


				$('#modal_grid_continer2').empty();

				$('#header_modal_text2').text('Recurso');

				$('#modal_grid_continer2').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');


				var u = new ModeloResource($(this).text(),'','','','','','','','','','');
				u.getForID().then(respuesta => {

				var v = new VistaGesResource('#divL'+contPages, respuesta);
				contPages++;
				v.loadHTML('../html/vista-ges-recursos.html').then(function(){
					v.loadContent().then(function(){
						console.log('desde un usuario con el role de ',t.userSession.role);
						if (t.userSession.role != 'ADMIN'){
							$('#ip_input').prop("disabled", true);
							$('#nombre_input').prop("disabled", true);
							$('#descripcion_input').prop("disabled", true);
							$('#select_rank').prop("disabled", true);
							$('#btnGuardarPerfil').prop("disabled", true);
							$('#btnResetPerfil').prop("disabled", true);
							$('#footer_recurso').css('display', 'none');

						}
						v.loadEvents();
						v.fadeInView();
					});
				});

			}).catch(e => {
    		alert("El usuario ya no se encuentra registrado en ActiveSpace",'','', "Cerrar");
			});


			});

	},


};
