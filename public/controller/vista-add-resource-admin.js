var VistaPerfilAddResource = function(id){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.userSession = '';
	this.btnGuardarPerfil ='#btnGuardarPerfil';
	this.btnResetPerfil = '#btnResetPerfil';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.role_selected = 0;
	this.rank_selected = 1;
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/checked.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha añadido al recurso</h6><small></small></div></div>';

}
VistaPerfilAddResource.prototype = {
	constructor: VistaPerfilAddResource,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-perfil-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("perfil del recurso: "+JSON.stringify(t.userSession));
		t.loadContent();
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},

	loadEvents: function(){
		//this.eventInputs();
		this.eventButtons();


	},

	loadContent: function (){
		var t = this;

		console.log($('#divMain'));

	},

	eventButtons: function(){
		var t = this;
		$(t.btnGuardarPerfil).off().on('click', function(){

			$(t.classDesactivateClick).addClass('desactivarClick');

			var selected_rank;

			if ($('#select_rank').val() == 3){
				selected_rank = 3;
			}else if ($('#select_rank').val() == 2){
				selected_rank = 2;
			}else if ($('#select_rank').val() == 1){
				selected_rank = 1;
			}else{
				selected_rank = 1;
			}

			//aqui comprobamos la conexión e iniciamos en un modo de conexión u otro

			if($('#nombre_input').val() !='' && $('#ip_input').val() !=''){

					var  newUser= {
						"ip_plug": $('#ip_input').val(),
			      "name": $('#nombre_input').val(),
			      "description": $('#descripcion_input').val(),
			      "rank": selected_rank,
			      "available": true,
			      "geometry": ''
					};
					console.log("usuario registrado ----- ", newUser);
					var u = new ModeloResource('',newUser.ip_plug,newUser.name,newUser.description,newUser.rank,newUser.available,newUser.geometry);

							u.addResource().then(respuesta => {
								console.log("recurso registrado ----- "+JSON.stringify(respuesta));
								if (respuesta != undefined ){
									$(t.classDesactivateClick).addClass('desactivarClick');

									$('#btn_dropdown_resources').click();
									//$('#btn_close_modal').click();
									$('#modal_grid_continer').append(t.alert_green_perfil);

								}else {
									$(t.classDesactivateClick).removeClass('desactivarClick');
									alert("Error al registrar",'','', "Cerrar");
								}
							});

			}else{
				$(t.classDesactivateClick).removeClass('desactivarClick');
				alert("Rellene todos los campos correctamente.",'','', "Cerrar");
			}

		});

		$(t.btnResetPerfil).on('click', function(){
			$.xPromptQ({header: '¿Estas seguro resetear a este usuario?'}, function(i){
		    console.log(i)
				if (i==true){
					$('#btn_close_modal').click();
				}
		  });

		});


	},


};
