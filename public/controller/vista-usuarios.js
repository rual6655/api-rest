var VistaGesUsers = function(id, sessionDashboard){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.sessionDashboard = sessionDashboard;
	this.userSession = sessionDashboard.userSession;
	this.list_users = "";
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/checked.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha actulizado la información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.modal_grid_continer = '<!-- Modal --><div class="modal fade bd-example-modal-lg" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h5 id="header_modal_text" class="modal-title" id="exampleModalLongTitle">Modal title</h5><button id="btn_close_modal" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="modal_grid_continer" class="container-fluid"></div></div></div></div></div>'
	this.modal_grid_continer2 = '<!-- Modal --><div class="modal fade bd-example-modal-lg" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h5 id="header_modal_text2" class="modal-title" id="exampleModalLongTitle2">Modal title</h5><button id="btn_close_modal2" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="modal_grid_continer2" class="container-fluid"></div></div></div></div></div>'

}
VistaGesUsers.prototype = {
	constructor: VistaGesUsers,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-perfil-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log($('#divMain'));
		//$('body').append(t.modal_grid_continer);
		$('body').append(t.modal_grid_continer2);

	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},

	loadContent: function(respuesta){
		var t= this;
		t.list_users=respuesta;
		return new Promise(function(resolve, reject) {
			console.log('el numero de usuario cargados: '+t.list_users.length);
			if(respuesta.length <= 1){
				t.eventButtons(0);
			}else{
				for(i=0; i<respuesta.length; i++){

					if (t.list_users[i].rank == 1){
						var rank_user= 'Público';
					}else if (t.list_users[i].rank == 3){
						var rank_user= 'Especial';

					}else{
						var rank_user= 'Limitado';

					}

					if (t.list_users[i]._id == t.userSession._id){

					}else{
						$('#contenedor_cards').append('<div id="card_user_'+i+'" class="card_contenedor col-12 col-sm-6 col-md-4 '+t.list_users[i].role+' '+t.list_users[i].rank+'"><div class="card"><div id="user_number_'+i+'" class="card-header" style="color: black;">'+t.list_users[i].name+'&'+t.list_users[i].email+'</div><div class="card-body" style="color: darkgray;"><form action="" method="post" enctype="multipart/form-data" class="form-horizontal"><div class="row form-group"><div class="col-md-3"><label class="">Role</label></div><div class="col-12 col-md-9"><p id="user_number_'+i+'_role" class="form-control-static ">'+t.list_users[i].role+'</p></div></div><div class="row form-group"><div class="col-md-3"><label class="">Rango</label></div><div class="col-12 col-md-9"><p id="user_number_'+i+'_rango" class="form-control-static ">'+rank_user+'</p></div></div></form></div></div><div class="card-footer"><button id="btnModificarUser_'+i+'" value='+i+' type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalLong">Modificar<button id="btnVerHistorial_'+i+'" value='+i+' type="submit" style="background-color: green" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-dot-circle-o"></i>Historial</button><button id="btnEliminarUser_'+i+'" value='+i+' type="reset" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i>Eliminar</button></div></div>');
						t.eventButtons(i);
					}
				}
			}

			resolve();
	});
	},

	eventButtons: function(i){
		var t = this;

		$('#mostrar_users').off().on('click', function(){
				$('.card_contenedor').css('display', 'none');
				console.log('mostrar por:', $('#filtro_ges_user').val());
			if($('#filtro_ges_user').val() == 1){
				$('.ESTUDIANTE').css('display', '');
			}else if($('#filtro_ges_user').val() == 2){
				$('.PROFESOR').css('display', '');
			}else if($('#filtro_ges_user').val() == 3){
				$('.1').css('display', '');
			}else if($('#filtro_ges_user').val() == 4){
				$('.2').css('display', '')
			}else if($('#filtro_ges_user').val() == 5){
				$('.3').css('display', '')
			}else if($('#filtro_ges_user').val() == 'Choose...'){
				$('.card_contenedor').css('display', '')
			}
		});


		$('#btnModificarUser_'+i+'').off().on('click', function(){
			console.log(this.value);
			console.log('El usuario a modificar: ',t.list_users[this.value]);
			$('#modal_grid_continer').empty();

			$('#header_modal_text').text('Modificar perfil');

			$('#modal_grid_continer').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaPerfilGesUser('#divL'+contPages, t.list_users[this.value]);
			contPages++;
			v.loadHTML('../html/vista-perfil_admin_user.html').then(function(){
				v.loadContentExtraResources().then(function(){
					v.loadContent().then(function(){
						v.loadEvents();
						v.fadeInView();
					});
				})

			});

		});

		$('#add_user_admin_perfil').off().on('click', function(){

			$('#modal_grid_continer').empty();

			$('#header_modal_text').text('Añadir Usuario');

			$('#modal_grid_continer').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaPerfilAddUser('#divL'+contPages, '');
			contPages++;
			v.loadHTML('../html/vista-perfil_admin.html').then(function(){
					v.loadContent();
					$('#email_input').removeAttr("disabled");
					v.loadEvents();
					v.fadeInView();
				});

		});

		$('#btnVerHistorial_'+i+'').off().on('click', function(){
			console.log(this.value);
			console.log('Mostrar uso por usuario: ',t.list_users[this.value]);
			$('#modal_grid_continer').empty();

			$('#header_modal_text').text('Historial usuario');

			$('#modal_grid_continer').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaUseResource('#divL'+contPages, t.list_users[this.value], 'ges_usuarios');
			contPages++;
			var u = new ModeloUser(t.list_users[this.value]._id,t.list_users[this.value].name,t.list_users[this.value].surname,t.list_users[this.value].email,t.list_users[this.value].role,t.list_users[this.value].rank,t.list_users[this.value].available,t.list_users[this.value].geometry,t.list_users[this.value].extraResources,t.list_users[this.value].pass,t.list_users[this.value].url);
			u.getHistorial().then(respuesta => {
				console.log('respuesta historial users-------------',respuesta);
				v.loadHTML('../html/vista-uso-recurso.html').then(function(){
					v.loadContent(respuesta).then(function(){
						v.fadeInView();
					});
				});
			});

		});

		$('#btnEliminarUser_'+i+'').off().on('click', function(){
			console.log(this.value);
			console.log('El usuario a eliminar',t.list_users[this.value]);
			var id_user_delete=t.list_users[this.value]._id;


		  $.xPromptQ({header: '¿Estas seguro de eliminar a este usuario?'}, function(i){
		    console.log(i)
				if (i==true){
					var u = new ModeloUser(id_user_delete,'','','','','','','','','','');
					u.delete().then(respuesta => {
						$('#btn_dropdown_users').click();
					});
				}
		  });
		});

	},

};
