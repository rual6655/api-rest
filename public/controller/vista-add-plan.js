var VistaAddPlan = function(id, sessionDashboard, respuesta){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.userSession = sessionDashboard;
	this.btnGuardarPlan ='#btnGuardarPlan';
	this.btnResetPlan = '#btnResetPlan';
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>plan</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha creado el evento con exito.</h6><small>Plan</small></div></div>';
	this.list_resources = respuesta;
}
VistaAddPlan.prototype = {
	constructor: VistaAddPlan,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-perfil-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("perfil del usuario: "+JSON.stringify(t.userSession));
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},
	loadEvents: function(){
		//this.eventInputs();
		this.eventButtons();


	},

	loadContent: function (dia,hora){
		var t = this;
		return new Promise(function(resolve, reject) {
			console.log(dia);
			$('#date1').val(dia);
			$('#date2').val(dia);
			$('#time1').val(hora);
			$('#time2').val(hora);

			for (var i=0; i<t.list_resources.length; i++){
				if (t.list_resources[i].rank<=t.userSession.rank){
					//if (t.list_resources[i].available == true){
						$('#multiple_select').append('<option value='+t.list_resources[i]._id+'>'+t.list_resources[i].ip_plug+'--Recurso: '+t.list_resources[i].name+'</option>');
					//}
				}
				for (var j=0; j<t.userSession.extraResources.length; j++){
					if (t.userSession.extraResources[j] == t.list_resources[i].ip_plug){
						$('#multiple_select').append('<option value='+t.list_resources[i]._id+'>'+t.list_resources[i].ip_plug+'--Recurso: '+t.list_resources[i].name+'</option>');
					}
				}
			}

			resolve();
		});
	},

	eventButtons: function(){
		var t = this;
		$(t.btnGuardarPlan).on('click', function(){
			$(t.classDesactivateClick).addClass('desactivarClick');
			var startTime, endTime;
			startTime = $('#date1').val()+'T'+$('#time1').val()+':00.000Z';
			endTime = $('#date2').val()+'T'+$('#time2').val()+':00.000Z';
			console.log(date_reloj+'+++++++++++++++'+ startTime+'++++++++++++++++++', (new Date(date_reloj) - new Date (startTime)));
			if ($('#multiple_select').val()!='0'){
			if((new Date(date_reloj) - new Date (startTime)) <= 0 ){
					if ((new Date (startTime) - new Date(endTime))  < 0){
						if ((new Date (startTime) - new Date(endTime))  > -57600000){



					//if($('#date').val()== '' || $('#date2').val()== '' || $('#time').val()== '' || $('#time2').val()== '' || $('#multiple_select').val() == 0){
						var u = new ModeloPlan('', startTime, endTime, $('#multiple_select').val(), t.userSession._id);

						u.addPlan().then(respuesta => {
							$(t.classDesactivateClick).removeClass('desactivarClick');
							console.log("Plan añadido ----- "+JSON.stringify(respuesta));
							alert('Añadido plan con día: '+new Date (respuesta.date.startTime).toUTCString()+'-' +new Date (respuesta.date.startTime).toUTCString(), '', '', "Cerrar");
							$('#btn_eventos_calendario').click();
							$('#btn_close_modal2').click();
						}).catch(e => {
							$(t.classDesactivateClick).removeClass('desactivarClick');
							console.log(e);
							alert('Error: No puedes usar el recurso o el recurso no está disponible.','','', "Cerrar");
						});
					/*}else{
						$(t.classDesactivateClick).removeClass('desactivarClick');
						alert("Edite algún campo.",'','', "Cerrar");
					}*/
					}else{
						$(t.classDesactivateClick).removeClass('desactivarClick');
						alert('No seas caradura y no acapares para ti sólo el recurso xD.','','', "Cerrar");

					}
				}else{
					$(t.classDesactivateClick).removeClass('desactivarClick');
					alert('La fecha final tiene que ser posterior a la de inicio del evento.','','', "Cerrar");

				}

			}else{
				$(t.classDesactivateClick).removeClass('desactivarClick');
				alert('La fecha de comienzo ya se ha pasado.','','', "Cerrar");
			}
		}else{
			$(t.classDesactivateClick).removeClass('desactivarClick');
			alert('Selecciona un recurso','','', "Cerrar");
		}
		});

		$(t.btnResetPlan).on('click', function(){

			$.xPromptQ({header: '¿Cancelar reserva?'}, function(i){
		    console.log(i)
				if (i==true){
					$('#btn_close_modal2').click();
				}
		  });
		});


	},


};
