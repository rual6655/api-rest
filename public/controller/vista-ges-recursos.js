var VistaGesResource = function(id, sessionUserList){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.userSession = sessionUserList;
	this.btnGuardarPerfil ='#btnGuardarPerfil';
	this.btnResetPerfil = '#btnResetPerfil';
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+sessionUserList.name+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionUserList.name+'</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/checked.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha actulizado la información para el recurso</h6><small>Recurso: '+sessionUserList.name+'</small></div></div>';
	this.role_selected = 0;
	this.rank_selected = 1;

}
VistaGesResource.prototype = {
	constructor: VistaGesResource,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-recursos-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("recurso : "+JSON.stringify(t.userSession));
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},
	loadEvents: function(){

		this.eventButtons();


	},


	loadContent: function (){
		var t = this;
return new Promise(function(resolve, reject) {
		console.log($('#divMain'));
		$('#ip_input').val(t.userSession.ip_plug);
		$('#nombre_input').val(t.userSession.name);
		$('#descripcion_input').val(t.userSession.description);


		if (t.userSession.rank == 1){
			$('#select_rank').val(1);
			t.rank_selected = 1;
		}else if (t.userSession.rank == 3){
			$('#select_rank').val(3);
			t.rank_selected = 3;
		}else{
			$('#select_rank').val(2);
			t.rank_selected = 2;
		}



		resolve();
	});
	},

	eventButtons: function(){
		var t = this;
		$(t.btnGuardarPerfil).on('click', function(){

			$(t.classDesactivateClick).addClass('desactivarClick');

			if ($('#select_rank').val() == 1){
				t.userSession.rank = 1;
			}else if ($('#select_rank').val() == 3){
				t.userSession.rank = 3;
			}else{
				t.userSession.rank = 2;
			}

			if(t.userSession.name != $('#nombre_input').val().toUpperCase() || t.userSession.ip_plug != $('#ip_input').val() || t.userSession.description != $('#descripcion_input').val() || t.rank_selected != $('#select_rank').val()){

			var u = new ModeloResource(t.userSession._id, $('#ip_input').val(),$('#nombre_input').val(),$('#descripcion_input').val(),$('#select_rank').val(),t.userSession.available,t.userSession.geometry);

			u.update().then(respuesta => {
				console.log("Recurso actualizado ----- "+JSON.stringify(respuesta));
				if (respuesta != undefined ){
					$(t.classDesactivateClick).addClass('desactivarClick');

						$('#btn_dropdown_resources').click();
						$('#modal_grid_continer').append(t.alert_green_perfil);


				}else {

					$(t.classDesactivateClick).removeClass('desactivarClick');
					alert("Error al modoficar",'','', "Cerrar");
				}
			});
		}else{

			$(t.classDesactivateClick).removeClass('desactivarClick');
			alert("Edite algún campo.",'','', "Cerrar");
		}
		});

		$(t.btnResetPerfil).on('click', function(){
			$.xPromptQ({header: '¿Estas seguro resetear este recurso?'}, function(i){
		    console.log(i)
				if (i==true){
					$('#btn_close_modal').click();
				}
		  });

		});


	},


};
