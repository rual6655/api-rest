var VistaHome = function(id, userSession){
	this.id = $(id);
	this.userSession = userSession;
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+userSession.email+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+userSession.email+'</small></div></div>';
	this.alert_red_perfil_role = '<div id="alert_red_perfil_role" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">No tienes asignado un role para tu cuenta de ActiveSpace. Contacta con el administrador del sistema.</h6><small>Perfil: '+userSession.email+'</small></div></div>';
	this.div_continer_white =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.div_continer_white_calendar =	'<div id="div_continer_white_calendar" class="d-flex align-items-center p-3 my-3 bg-white rounded shadow-sm"></div>';
	this.classDesactivateClick = '.divContenedor';
	this.modal_grid_continer2 = '<!-- Modal --><div class="modal fade bd-example-modal-lg" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h5 id="header_modal_text2" class="modal-title" id="exampleModalLongTitle2">Modal title</h5><button id="btn_close_modal2" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="modal_grid_continer2" class="container-fluid"></div></div></div></div></div>'

}
VistaHome.prototype = {
	constructor: VistaHome,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('vista home L->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
	    var fileref=document.createElement("link");
	    fileref.setAttribute("rel", "stylesheet");
	    fileref.setAttribute("type", "text/css");
	    fileref.setAttribute("href", css_file);
	    document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("iniciado sesión con usuario: "+JSON.stringify(t.userSession));
		console.log("iniciado sesión con usuario: "+JSON.stringify(t.userSession._id));
		$('.NamePerfil').text(t.userSession.name+'&'+t.userSession.email);
		console.log('el contenedor Main contiene:' , $('#divMain'));
		$('body').append(t.modal_grid_continer2);
	},
	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	loadContent: function(){
		var t= this;

		return new Promise(function(resolve, reject) {
		var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
		u.getPlannings().then(respuesta => {
			console.log(respuesta.length);
			$('#nEventos').text(respuesta.length);
			resolve();
		}).catch(e => {
			alert("ERROR",'','', "Cerrar");
			reject();
		});

	});
	},
	loadEvents: function(){
		var t=this;
		return new Promise(function(resolve, reject) {

			$('#btnGeo').on('click', function(){

				$(t.classDesactivateClick).addClass('desactivarClick');

				$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
				var v = new VistaGeo('#divL'+contPages);

				contPages++;

				v.loadcss('../css/home-geo.css');
				v.loadHTML('../html/vista-geo.html').then(function(){
				 v.loadEvents();
				 v.fadeInView();

				});
			});

			$('.NamePerfil, #btn_navbar_dark').off().on('click', function(){
				var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
				u.getPlannings().then(respuesta => {
					console.log(respuesta.length);
					$('#nEventos').text(respuesta.length);
				}).catch(e => {
					alert("ERROR",'','', "Cerrar");
				});

				if ($('#navbarsExampleDefault').hasClass("open")){
					$('#navbarsExampleDefault').removeClass("open");
					$('#navbar_white').css('display', '');
					$('#divMain').empty();
				}else{
					$('#navbar_white').css('display', '');
					$('#divMain').empty();
				}
			});

			$('.perfilUser').off().on('click', function(){
				$('#modal_grid_continer2').empty();
				$('#divMain').empty();
				t.load_div_perfil();
			});

			$('#btn_eventos_calendario').off().on('click', function(){
				var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
				u.getPlannings().then(respuesta => {
					console.log(respuesta.length);
					$('#nEventos').text(respuesta.length);
				}).catch(e => {
					alert("ERROR",'','', "Cerrar");
				});
				$('#divMain').empty();
				t.load_calendar();
			});

		$('.salir').off().on('click', function(){

			$(t.classDesactivateClick).addClass('desactivarClick');

			$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
			var v = new VistaLogin('#divL'+contPages);
				contPages++;
				t.moveOut();
				v.loadcss('../css/login.css');
						v.loadHTML('../html/vista-Login.html').then(function(){
							v.loadEvents();
							v.fadeInView();

						});
		});
		$('.tutorial').off().on('click', function(){
				$('#divMain').empty();
				$('#divMain').load('../html/vista-tutorial.html', function(response, status, xhr){
					if(status == "error"){
						alert("Error. "+shr.status + " " + xhr.statusText);
						reject();
					}else{
						console.log('Vista-Controller-login-LoadHTML->',' html cargado');
						$('#navbarsExampleDefault').removeClass("open");
					}
				});
		});
		$('.sobre').off().on('click', function(){
				$('#divMain').empty();
				$('#divMain').load('../html/vista-sobre.html', function(response, status, xhr){
					if(status == "error"){
						alert("Error. "+shr.status + " " + xhr.statusText);
						reject();
					}else{
						console.log('Vista-Controller-login-LoadHTML->',' html cargado');
						$('#navbarsExampleDefault').removeClass("open");
					}
				});
		});

		$('#user_recursos').off().on('click', function(){

			$(t.classDesactivateClick).addClass('desactivarClick');
			$('#exampleModalLong').remove();
			var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
			u.getPlannings().then(respuesta => {
				console.log(respuesta.length);
				$('#nEventos').text(respuesta.length);
			}).catch(e => {
				alert("ERROR",'','', "Cerrar");
			});

				$('#divMain').empty();
				//$('#navbar_white').css('display', 'none');
				//$('#divMain').append(t.barra_filtro_resources);
				$('#divMain').append(t.div_continer_white);

				$('#div_continer_white').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
				$('#divL'+contPages).css('width', 'auto');

				var v = new VistaGesResourcesFromUser('#divL'+contPages, t);
				contPages++;


				var u = new ModeloResource('','','','','','','');
				u.get().then(respuesta => {
					v.loadHTML('../html/vista-recursos.html').then(function(){
						v.loadContent(respuesta).then(function(){

							$('#navbarsExampleDefault').removeClass("open");
							v.fadeInView();
						});
					});
				});
		});
		$('#user_reservas').off().on('click', function(){
			$(t.classDesactivateClick).addClass('desactivarClick');
			var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
			u.getPlannings().then(respuesta => {
				console.log(respuesta.length);
				$('#nEventos').text(respuesta.length);
			}).catch(e => {
				alert("ERROR",'','', "Cerrar");
			});

				$('#divMain').empty();
				//$('#navbar_white').css('display', 'none');
				//$('#divMain').append(t.barra_filtro_resources);
				$('#divMain').append(t.div_continer_white);

				$('#div_continer_white').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
				var v = new VistaUseResource('#divL'+contPages, t.userSession, 'user');
				contPages++;
				console.log('este es el usuario con la sesión actual: ----- ',t.userSession);
				var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
				u.getPlannings().then(respuesta => {
					console.log('respuesta historial users-------------',respuesta);
					v.loadHTML('../html/vista-uso-recurso.html').then(function(){
						v.loadContent(respuesta).then(function(){
							v.fadeInView();
						});
					});
				}).catch(e => {
					alert("ERROR",'','', "Cerrar");

				});

		});

		if (t.userSession.role == undefined || t.userSession.role == ""){
			t.load_div_perfil_init();
		}else{
				$('#user_recursos').click();
		}


		resolve();
	});
	},
	load_div_perfil_init: function(){
		var t=this;

		$('#navbar_white').css('display', 'none');


		if (t.userSession.role == undefined || t.userSession.role == ""){
			$('#divMain').append(t.alert_red_perfil);
			$('#divMain').append(t.alert_red_perfil_role);
		}else{
			$('#divMain').append(t.alert_red_perfil);
		}

		$('#divMain').append(t.div_continer_white);


		$('#div_continer_white').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
		var v = new VistaPerfil('#divL'+contPages, t);

		contPages++;

		v.loadHTML('../html/vista-perfil.html').then(function(){
			v.loadContent().then(function(){
				v.loadEvents();
				$('#navbarsExampleDefault').removeClass("open");
				v.fadeInView();
			});

		});
	},

	load_div_perfil: function(){
		var t=this;

		if (t.userSession.role == undefined || t.userSession.role == ""){
			t.load_div_perfil_init();
		}else{

			$(t.classDesactivateClick).addClass('desactivarClick');


			$('#navbar_white').css('display', 'none');

			$('#divMain').append(t.div_continer_white);


			$('#div_continer_white').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaPerfil('#divL'+contPages, t);

			contPages++;

			v.loadHTML('../html/vista-perfil.html').then(function(){
				v.loadContent().then(function(){
					v.loadEvents();
		 			$('#navbarsExampleDefault').removeClass("open");
		 			v.fadeInView();
				});
			});

		}

	},
	load_calendar: function(){
		var t= this;

			$(t.classDesactivateClick).addClass('desactivarClick');

			//$('#navbar_white').css('display', 'none');

			$('#divMain').append(t.div_continer_white_calendar);

			$('#div_continer_white_calendar').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');
			var v = new VistaCalendario('#divL'+contPages, t);

			contPages++;
			v.loadEvents();

			$(t.classDesactivateClick).removeClass('desactivarClick');

	},

};
