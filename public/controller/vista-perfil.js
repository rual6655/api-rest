var VistaPerfil = function(id, sessionDashboard){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.sessionDashboard = sessionDashboard;
	this.userSession = sessionDashboard.userSession;
	this.btnGuardarPerfil ='#btnGuardarPerfil';
	this.btnResetPerfil = '#btnResetPerfil';
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/checked.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha actulizado la información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.role_selected = 0;
	this.rank_selected = 1;

}
VistaPerfil.prototype = {
	constructor: VistaPerfil,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-perfil-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("perfil del usuario: "+JSON.stringify(t.userSession));
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},
	loadEvents: function(){
		//this.eventInputs();
		this.eventButtons();


	},
	eventInputs: function(){
		var t= this;
		$(t.password+','+t.email).keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if ((code==13)){
				$(t.btnRegistro).click();
			}
			return true;
		});
	},

	loadContent: function (){
		var t = this;
return new Promise(function(resolve, reject) {
		console.log($('#divMain'));
		$('.emailName').text(t.userSession.email+"&"+t.userSession.name);
		$('#nombre_input').val(t.userSession.name);
		$('#apellido_input').val(t.userSession.surname);
		$('#email_input').val(t.userSession.email);
		$('#password_input').val(t.userSession.pass);

		if (t.userSession.role == "ADMIN"){
			$('#select_role').val(3);
			t.role_selected = 3;
		}else if (t.userSession.role == "PROFESOR"){
			$('#select_role').val(2);
			t.role_selected = 2;
		}else if (t.userSession.role == "ESTUDIANTE"){
			$('#select_role').val(1);
			t.role_selected = 1;
		}else{
			$('#select_role').val(0);
			t.role_selected = 0;
		}


		if (t.userSession.rank == 1){
			$('#select_rank').val(1);
			t.rank_selected = 1;
		}else if (t.userSession.rank == 3){
			$('#select_rank').val(3);
			t.rank_selected = 3;
		}else{
			$('#select_rank').val(2);
			t.rank_selected = 2;
		}

		for (var i=0; i<t.userSession.extraResources.length; i++){
			var j= i+1;
			$('#multiple_select').append('<option value='+j+'>'+t.userSession.extraResources[i]+'</option>');
		}
		console.log('este es el id del usuario en la base de datos: '+t.userSession._id);
		resolve();
	});
	},

	eventButtons: function(){
		var t = this;
		$(t.btnGuardarPerfil).on('click', function(){

			$(t.sessionDashboard.classDesactivateClick).addClass('desactivarClick');
			$(t.classDesactivateClick).addClass('desactivarClick');

			if ($('#select_role').val() == 3){
				t.userSession.role = "ADMIN";
			}else if ($('#select_role').val() == 2){
				t.userSession.role ="PROFESOR"
			}else if ($('#select_role').val() == 1){
				t.userSession.role ="ESTUDIANTE"
			}else{
				t.userSession.role = "";
			}

			if ($('#select_rank').val() == 1){
				t.userSession.rank = 1;
			}else if ($('#select_rank').val() == 3){
				t.userSession.rank = 3;
			}else{
				t.userSession.rank = 2;
			}

			if(t.userSession.name != $('#nombre_input').val().toUpperCase() || t.userSession.surname != $('#apellido_input').val().toUpperCase() || t.userSession.pass != $('#password_input').val() || t.role_selected != $('#select_role').val()|| t.rank_selected != $('#select_rank').val()){

			var pass_encrypted = CryptoJS.AES.encrypt($('#password_input').val(), "@1e");

			if (t.userSession.pass == $('#password_input').val()){
					var u = new ModeloUser(t.userSession._id,$('#nombre_input').val(),$('#apellido_input').val(),t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,'');
			}else{
					var u = new ModeloUser(t.userSession._id,$('#nombre_input').val(),$('#apellido_input').val(),t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,pass_encrypted.toString(),'');
			}

			u.update().then(respuesta => {
				console.log("Perfil actualizado ----- "+JSON.stringify(respuesta));
				if (respuesta != undefined ){
					$(t.classDesactivateClick).addClass('desactivarClick');

						var  newUser= {
							"_id": t.userSession._id,
			        "name":  respuesta.name ,
			        "surname": respuesta.surname,
			        "email": respuesta.email,
			        "pass": respuesta.pass,
			        "role": respuesta.role,
			        "rank": respuesta.rank,
			        "available": respuesta.available,
			        "geometry": respuesta.geometry,
			        "extraResources": respuesta.extraResources
			      };

						if (respuesta.role != "ADMIN"){
							$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
							var v = new VistaHome('#divL'+contPages, newUser);

							contPages++;
							t.sessionDashboard.moveOut();
							v.loadcss('../css/canvasCSS/offcanvas.css');
							v.loadHTML('../html/vista-Home.html').then(function(){
								v.loadEvents();
								v.fadeInView();
								$('#divMain').empty();
								$('#navbar_white').css('display', '');
								$('#divMain').append(t.alert_green_perfil);
								v.load_div_perfil();
							});

						}else{
							$('body').append('<div id="divL'+contPages+'" class="divContenedor" </div>');
							var v = new VistaHomeAdmin('#divL'+contPages, newUser);

							contPages++;
							t.sessionDashboard.moveOut();
							v.loadcss('../css/canvasCSS/offcanvas.css');
							v.loadHTML('../html/vista-HomeAdmin.html').then(function(){
								v.loadEvents();
								v.fadeInView();
								$('#divMain').empty();
								$('#navbar_white').css('display', '');
								$('#divMain').append(t.alert_green_perfil);
								v.load_div_perfil();
							});
						}


				}else {
					$(t.sessionDashboard.classDesactivateClick).removeClass('desactivarClick');
					$(t.classDesactivateClick).removeClass('desactivarClick');
					alert("Error al modoficar",'','', "Cerrar");
				}
			});
		}else{
			$(t.sessionDashboard.classDesactivateClick).removeClass('desactivarClick');
			$(t.classDesactivateClick).removeClass('desactivarClick');
			alert("Edite algún campo.",'','', "Cerrar");
		}
		});

		$(t.btnResetPerfil).on('click', function(){
			$(t.sessionDashboard.classDesactivateClick).addClass('desactivarClick');
			$('#divMain').empty();
			$('#divMain').append(t.alert_green_perfil);
			t.sessionDashboard.load_div_perfil();

		});


	},


};
