var VistaCalendario = function(id, sessionDashboard){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.divDashboard = sessionDashboard;
	this.userSession = sessionDashboard.userSession;
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha actulizado la información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionDashboard.userSession.email+'</small></div></div>';
	this.eventos= [];
	this.eventosMios= [];
	this.eventosRecursos= [];
	this.eventosCalendar = [];
	this.eventosCalendarMios = [];
	this.eventosCalendarRecursos = [];
}
VistaCalendario.prototype = {
	constructor: VistaCalendario,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-perfil-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log($('#divMain'));
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},

	loadEvents: function(){

		var t = this;
		//t.id.append('<div id="external-events" style="z-index: 2;top: 20px;left: 20px;width: 150px;padding: 0 10px;border: 1px solid #ccc;background: #eee;"><p><strong>Draggable Events</strong></p><div class="fc-event">My Event 1</div><div class="fc-event">My Event 2</div><div class="fc-event">My Event 3</div><div class="fc-event">My Event 4</div><div class="fc-event">My Event 5</div><p><input type="checkbox" id="drop-remove"><label for="drop-remove">remove after drop</label></p></div>');

		t.id.append('<div id="div_continer_recursos_calendar" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"><div><div class="fc-button-group"><button id="btnMes" type="button" class="fc-dayGridMonth-button fc-button fc-button-primary fc-button-active">month</button><button id="btnSemana" type="button" class="fc-timeGridWeek-button fc-button fc-button-primary">week</button><button id="btnDia" type="button" class="fc-timeGridDay-button fc-button fc-button-primary">day</button><button id="btnLista" type="button" class="fc-listWeek-button fc-button fc-button-primary">list</button></div></div><div class="col form-group"><div class=""><label for="multiple-select" style="color: black;" class="float-right">Mostrar reservas del recurso:</label></div><div id="select_recursos" class=""><select  id="multiple_select_recursos" class="form-control"><option value= 0 >Selecciona ...</option></select></div><button id="btnMotrarMasRecursos" class="float-right">Mostrar</button></div></div>');



		t.id.append('<div id="calendar" style="background-color: white"></div>');

		t.loadeventsCalendar().then(function(){
		t.loadeventsCalendarTodos().then(function(){
		t.loadcontent().then(function(){
		t.loadcontentTodos().then(function(){
				t.cargarReservasOtrosRecursos().then(function(){
				t.loadEventListener();
		});
		});
		});
		});
		});


	},

	cargarReservasOtrosRecursos: function(){
		var t= this;
		return new Promise(function(resolve, reject) {
			var u = new ModeloResource('','','','','','','','','','','');
			u.get().then(respuesta => {
			for (var i=0; i<respuesta.length; i++){
				if (respuesta[i].rank<=t.userSession.rank){
					//if (respuesta[i].available == true){
						$('#multiple_select_recursos').append('<option value='+respuesta[i]._id+'>'+respuesta[i].ip_plug+'--Recurso: '+respuesta[i].name+'</option>');
					//}
				}
				for (var j=0; j<t.userSession.extraResources.length; j++){
					if (t.userSession.extraResources[j] == respuesta[i].ip_plug){
						$('#multiple_select_recursos').append('<option value='+respuesta[i]._id+'>'+respuesta[i].ip_plug+'--Recurso: '+respuesta[i].name+'</option>');
					}
				}
			}

			resolve();
		});
		});
	},

	loadeventTodosRecursos: function(e){
		var t =this;
		return new Promise(function(resolve, reject) {
		var r = new ModeloResource(e,"","","","","","");
			r.getHistorial().then(respuesta => {
				console.log('respuesta historial recursos-------------',respuesta);
				t.eventosRecursos = respuesta;
				resolve();
			});
		});
	},

	eventInputs: function(){
		var t= this;
		$(t.password+','+t.email).keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if ((code==13)){
				$(t.btnRegistro).click();
			}
			return true;
		});
	},

	loadeventsCalendar: function(){
		var t =this;
		return new Promise(function(resolve, reject) {
		console.log('buscando mierda de :',t.userSession)
		var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
			u.getPlannings().then(respuesta => {
				console.log('respuesta planning users-------------',respuesta);
				t.eventosMios = respuesta;
				resolve();
			});
		});
	},

	loadeventsCalendarTodos: function(){
		var t =this;
		return new Promise(function(resolve, reject) {
		console.log('buscando mierda de :',t.userSession)
		var u = new ModeloUser(t.userSession._id,t.userSession.name,t.userSession.surname,t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,t.userSession.url);
			u.getHistorial().then(respuesta => {
				console.log('respuesta historial users-------------',respuesta);
				t.eventos = respuesta;
				resolve();
			});
		});
	},



	loadcontent: function(){
		var t= this;
		return new Promise(function(resolve, reject) {
			for (var i =0; i<t.eventosMios.length; i++){
				t.eventosCalendarMios.push({
		      id  : t.eventosMios[i].resource_id,
		      start  : t.eventosMios[i].date.startTime,
		      end    : t.eventosMios[i].date.endTime,
					textColor : 'white'
		    })
			}

			resolve();
		});
	},

	loadcontentTodos: function(){
		var t= this;
		return new Promise(function(resolve, reject) {
			for (var i =0; i<t.eventos.length; i++){
					if( (new Date (t.eventos[i].date.endTime) - new Date(date_reloj)) >= 0 ){

					}else{
						t.eventosCalendar.push({
				      id  : t.eventos[i].resource_id,
				      start  : t.eventos[i].date.startTime,
				      end    : t.eventos[i].date.endTime,
							textColor : 'white',
							backgroundColor : 'red',
							borderColor : 'red'
				    });
					}
			}

			resolve();
		});
	},

	loadcontentTodosRecursos: function(){
		var t= this;
		return new Promise(function(resolve, reject) {
			for (var i =0; i<t.eventosRecursos.length; i++){
					if( (new Date (t.eventosRecursos[i].date.endTime) - new Date(date_reloj)) <= 0 ){

					}else{
						t.eventosCalendarRecursos.push({
				      id  : t.eventosRecursos[i].resource_id,
				      start  : t.eventosRecursos[i].date.startTime,
				      end    : t.eventosRecursos[i].date.endTime,
							textColor : 'white',
							backgroundColor : 'purple',
							borderColor : 'purple'
				    });
					}
			}

			resolve();
		});
	},

	loadEventListener: function(){
		var t= this;

	  var calendarEl = document.getElementById('calendar');
		var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: [ 'dayGrid', 'interaction', 'timeGrid', 'list', 'timeline', 'rrule' ],
			defaultView: 'dayGridMonth',
			selectable: true,
		  timeZone: 'UTC',
		  events: [
		  ],
		  dateClick: function(arg) {
		    console.log(arg.date.toUTCString());
				var today = new Date();

				var dia=''+new Date(arg.date.toUTCString()).getFullYear()+'-'+checkTime(new Date(arg.date.toUTCString()).getMonth()+1)+'-'+checkTime(new Date(arg.date.toUTCString()).getDate())
				var hora= ''+checkTime(new Date(today.toUTCString()).getHours())+':'+checkTime(new Date(today.toUTCString()).getMinutes()+2);
				console.log(hora);

					$('#modal_grid_continer2').empty();
					$('#exampleModalLong2').modal("show");
					$('#header_modal_text2').text('Evento');

					$('#modal_grid_continer2').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');


					var u = new ModeloResource('','','','','','','','','','','');
					u.get().then(respuesta => {
					console.log('vamos a crear un evento para el usuario: ', t.userSession);
					console.log('recursos del sistema ', respuesta);

					var v = new VistaAddPlan('#divL'+contPages, t.userSession, respuesta);
					contPages++;
					v.loadHTML('../html/vista-plan.html').then(function(){
						v.loadContent(dia,hora).then(function(){
							v.loadEvents();
							v.fadeInView();
						});
					});
				});


				function checkTime(i) {
				    if (i < 10) {
				        i = "0" + i;
				    }
				    return i;
				};

				 // use *UTC* methods on the native Date Object
		    // will output something like 'Sat, 01 Sep 2018 00:00:00 GMT'
		  },
			eventClick: function(i){
				console.log('hola', i.event.id);
				$('#modal_grid_continer2').empty();
				$('#exampleModalLong2').modal("show");
				$('#header_modal_text2').text('Recurso');

				$('#modal_grid_continer2').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');


				var u = new ModeloResource(i.event.id,'','','','','','','','','','');
				var fechainicio, fechafinal;

				u.getForID().then(respuesta => {

				var v = new VistaGesResource('#divL'+contPages, respuesta);
				contPages++;
				v.loadHTML('../html/vista-ges-recursos.html').then(function(){
					v.loadContent().then(function(){
						$('#card_body_recursos').append('<div class="row form-group"><div class="col-md-3"><label style="color: black; for="text-input" class="" >Fecha de inicio</label></div><div class="col-12 col-md-9"><input id="comienzo_fecha" name="text-input" placeholder="" type="text" class="form-control"><small class="form-text text-muted"></small></div></div><div class="row form-group"><div class="col-md-3"><label style="color: black; for="text-input" class="" >Fecha final</label></div><div class="col-12 col-md-9"><input id="final_fecha" name="text-input" placeholder="" type="text" class="form-control"><small class="form-text text-muted"></small></div></div>');
						$('#comienzo_fecha').val(i.event.start.toUTCString());
						$('#final_fecha').val(i.event.end.toUTCString());
						$('#comienzo_fecha').prop("disabled", true);
						$('#final_fecha').prop("disabled", true);
						console.log('desde un usuario con el role de ',t.userSession.role);
						if (t.userSession.role != 'ADMIN'){
							$('#ip_input').prop("disabled", true);
							$('#nombre_input').prop("disabled", true);
							$('#descripcion_input').prop("disabled", true);
							$('#select_rank').prop("disabled", true);
							$('#footer_recurso').css('display', 'none');

						}
						$('#footer_recurso').css('display', 'none');

						v.loadEvents();
						v.fadeInView();
					});
				});

			}).catch(e => {
				alert("El recurso ya no se encuentra registrado en ActiveSpace",'','', "Cerrar");
			});
			}
		});

		calendar.render();

		$('.fc-center').append('<button id="btnAñadirPlanning" type="button" class="btn btn-primary btn-sm" style="background-color: black; border-color: black;">Crear reserva</button>');

		for (var i=0; i<t.eventosCalendarMios.length; i++){
			calendar.addEvent(t.eventosCalendarMios[i]);
		}
		for (var i=0; i<t.eventosCalendar.length; i++){
			calendar.addEvent(t.eventosCalendar[i]);
		}

		$('#btnMotrarMasRecursos').on('click', function(){
				t.loadeventTodosRecursos($('#multiple_select_recursos').val()).then(function(){
					t.loadcontentTodosRecursos().then(function(){
						for (var i=0; i<t.eventosCalendarRecursos.length; i++){
							calendar.addEvent(t.eventosCalendarRecursos[i]);
						}
					});
				});
		});

		$('#btnMes').on('click', function(){
			//$('#calendar').remove();
			$('.fc-button').removeClass('fc-button-active');
			$('#btnMes').addClass('fc-button-active');

			calendar.changeView('dayGridMonth');
		});

		$('#btnSemana').on('click', function(){
			//$('#calendar').remove();
			$('.fc-button').removeClass('fc-button-active');
			$('#btnSemana').addClass('fc-button-active');

			calendar.changeView('dayGridWeek');

		});

		$('#btnDia').on('click', function(){
			//$('#calendar').remove();
			$('.fc-button').removeClass('fc-button-active');
			$('#btnDia').addClass('fc-button-active');

			calendar.changeView('dayGridDay');

		});

		$('#btnLista').on('click', function(){
			//$('#calendar').remove();
			$('.fc-button').removeClass('fc-button-active');
			$('#btnLista').addClass('fc-button-active');

			calendar.changeView('listWeek');
		});

		$('#btnAñadirPlanning').on('click', function(){
			$('#modal_grid_continer2').empty();
			$('#exampleModalLong2').modal("show");
			$('#header_modal_text2').text('Evento');

			$('#modal_grid_continer2').append('<div id="divL'+contPages+'" class="divContenedor container-fluid" </div>');


			var u = new ModeloResource('','','','','','','','','','','');
			u.get().then(respuesta => {
			console.log('vamos a crear un evento para el usuario: ', t.userSession);
			console.log('recursos del sistema ', respuesta);

			var v = new VistaAddPlan('#divL'+contPages, t.userSession, respuesta);
			contPages++;
			v.loadHTML('../html/vista-plan.html').then(function(){
				v.loadContent().then(function(){
					v.loadEvents();
					v.fadeInView();
				});
			});
		});
		});

	},

};
