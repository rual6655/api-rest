var VistaPerfilGesUser = function(id, sessionUserList){
	this.id = $(id);
	this.classDesactivateClick = '.divContenedor';
	this.userSession = sessionUserList;
	this.btnGuardarPerfil ='#btnGuardarPerfil';
	this.btnResetPerfil = '#btnResetPerfil';
	this.alert_red = '<div id="div_alert_red" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha producido un error</h6><small>Perfil: '+sessionUserList.email+'</small></div></div>';
	this.alert_red_perfil = '<div id="div_alert_red_perfil" class="d-flex align-items-center p-3 my-3 text-white-50 bg-red rounded shadow-sm"><img class="mr-3" src="../img/error.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Falta por añadir cierta información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionUserList.email+'</small></div></div>';
	this.div_white_continer =	'<div id="div_continer_white" class="d-flex align-items-center p-3 my-3 text-white-50 bg-white rounded shadow-sm"></div>';
	this.alert_green_perfil = '<div id="div_alert_green" class="d-flex align-items-center p-3 my-3 text-white-50 bg-green rounded shadow-sm"><img class="mr-3" src="../img/checked.svg" alt="" width="48" height="48"><div class="lh-100"><h6 class="mb-0 text-white lh-100">Se ha actulizado la información para tu cuenta de ActiveSpace</h6><small>Perfil: '+sessionUserList.email+'</small></div></div>';
	this.role_selected = 0;
	this.rank_selected = 1;
	this.list_extraResources_name = [];

}
VistaPerfilGesUser.prototype = {
	constructor: VistaPerfilGesUser,
	loadHTML: function(ruta){
		var idP = this.id;
		return new Promise(function(resolve, reject) {
			console.log('cargando contenedor vista: ', idP);
			idP.load(ruta, function(response, status, xhr){
				if(status == "error"){
					alert("Error. "+shr.status + " " + xhr.statusText);
					reject();
				}else{
					console.log('Vista-Controller-perfil-LoadHTML->',' html cargado');
					resolve();
				}
			});
		});
	},
	loadcss : function(css_file) {
		var fileref=document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", css_file);
		document.getElementsByTagName("head")[0].appendChild(fileref);
	},
	hideView: function(){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.css('display', 'none').empty();
	},
	fadeInView: function(){
		var t = this;
		$(t.classDesactivateClick).removeClass('desactivarClick');
		t.id.css('display', '');
		console.log("perfil del usuario: ",t.userSession);
	},

	moveOut: function(deviceWidth){
		var t = this;
		$(t.classDesactivateClick).addClass('desactivarClick');
		t.id.remove();
		$(t.classDesactivateClick).removeClass('desactivarClick');
	},
	getID: function(){
		return this.id;
	},
	loadEvents: function(){
		//this.eventInputs();
		this.eventButtons();


	},
	eventInputs: function(){
		var t= this;
		$(t.password+','+t.email).keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if ((code==13)){
				$(t.btnRegistro).click();
			}
			return true;
		});
	},

	loadContentExtraResources: function (){
		var t = this;
		return new Promise(function(resolve, reject) {
				var u = new ModeloResource('','','','','','','');
				u.get().then(respuesta => {
					//console.log(respuesta);
					respuesta.forEach(function(i) {
						t.list_extraResources_name.push(i);
					});
					resolve();
		  	});

		console.log(t.list_extraResources_name);

	});
	},
	loadContent: function (){
		var t = this;
return new Promise(function(resolve, reject) {
		console.log($('#divMain'));
		$('.emailName').text(t.userSession.email+"&"+t.userSession.name);
		$('#nombre_input').val(t.userSession.name);
		$('#apellido_input').val(t.userSession.surname);
		$('#email_input').val(t.userSession.email);
		$('#password_input').val(t.userSession.pass);
		$('#tagRFID').val(t.userSession.tag);

		if (t.userSession.role == "ADMIN"){
			$('#select_role').val(3);
			t.role_selected = 3;
		}else if (t.userSession.role == "PROFESOR"){
			$('#select_role').val(2);
			t.role_selected = 2;
		}else if (t.userSession.role == "ESTUDIANTE"){
			$('#select_role').val(1);
			t.role_selected = 1;
		}else{
			$('#select_role').val(0);
			t.role_selected = 0;
		}


		if (t.userSession.rank == 1){
			$('#select_rank').val(1);
			t.rank_selected = 1;
		}else if (t.userSession.rank == 3){
			$('#select_rank').val(3);
			t.rank_selected = 3;
		}else{
			$('#select_rank').val(2);
			t.rank_selected = 2;
		}

		for (var i=0; i<t.list_extraResources_name.length; i++){
			var x= 0;
			for (var j=0; j<t.userSession.extraResources.length; j++){
				if (t.list_extraResources_name[i].ip_plug == t.userSession.extraResources[j] || t.list_extraResources_name[i].rank<=t.userSession.rank){
					x=1;
				}
			}
			if (x==0){
				$('#multiple_select').append('<option value='+t.list_extraResources_name[i].ip_plug+'>'+t.list_extraResources_name[i].ip_plug+'--Recurso: '+t.list_extraResources_name[i].name+'</option>');

			}
		}
		console.log('este es el id del usuario en la base de datos: '+t.userSession._id);

		resolve();
	});
	},

	eventButtons: function(){
		var t = this;


		$(t.btnGuardarPerfil).on('click', function(){


			$(t.classDesactivateClick).addClass('desactivarClick');

			if ($('#select_role').val() == 3){
				t.userSession.role = "ADMIN";
			}else if ($('#select_role').val() == 2){
				t.userSession.role ="PROFESOR"
			}else if ($('#select_role').val() == 1){
				t.userSession.role ="ESTUDIANTE"
			}else{
				t.userSession.role = "";
			}

			if ($('#select_rank').val() == 1){
				t.userSession.rank = 1;
			}else if ($('#select_rank').val() == 3){
				t.userSession.rank = 3;
			}else{
				t.userSession.rank = 2;
			}


			if(t.userSession.name != $('#nombre_input').val().toUpperCase() || t.userSession.surname != $('#apellido_input').val().toUpperCase() || t.userSession.pass != $('#password_input').val() || t.role_selected != $('#select_role').val()|| t.rank_selected != $('#select_rank').val() || t.userSession.tag != $('#tagRFID').val() || t.userSession.extraResources.length != 0){

			var pass_encrypted = CryptoJS.AES.encrypt($('#password_input').val(), "@1e");

			if (t.userSession.pass == $('#password_input').val()){
					var u = new ModeloUser(t.userSession._id,$('#nombre_input').val(),$('#apellido_input').val(),t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,t.userSession.pass,$('#tagRFID').val());
			}else{
					var u = new ModeloUser(t.userSession._id,$('#nombre_input').val(),$('#apellido_input').val(),t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,t.userSession.extraResources,pass_encrypted.toString(),$('#tagRFID').val());
			}

			u.update().then(respuesta => {
				console.log("Perfil actualizado ----- "+JSON.stringify(respuesta));
				if (respuesta != undefined ){
					$(t.classDesactivateClick).addClass('desactivarClick');
					$('#btn_dropdown_users').click();
					$('#modal_grid_continer').append(t.alert_green_perfil);


				}else {

					$(t.classDesactivateClick).removeClass('desactivarClick');
					alert("Error al modificar",'','', "Cerrar");
				}
			});

		}else{

			$(t.classDesactivateClick).removeClass('desactivarClick');
			alert("Edite algún campo.",'','', "Cerrar");
		}
		});

		$('#btnGuardarRecurso').on('click', function(){

			var u = new ModeloUser(t.userSession._id,$('#nombre_input').val(),$('#apellido_input').val(),t.userSession.email,t.userSession.role,t.userSession.rank,t.userSession.available,t.userSession.geometry,$('#multiple_select').val(),t.userSession.pass,'');
			if ($('#multiple_select').val() != 0){
				u.updateResources().then(respuesta => {
					console.log("Recurso añadido a usuario ----- "+JSON.stringify(respuesta));
					if (respuesta != undefined ){
						$(t.classDesactivateClick).addClass('desactivarClick');

						$('#btn_dropdown_users').click();
						alert("Añadido recurso.",'','', "Cerrar");
						$('#btn_close_modal').click();

					}else {

						$(t.classDesactivateClick).removeClass('desactivarClick');
						alert("Error al modificar",'','', "Cerrar");
					}
				});
			}else{
				$(t.classDesactivateClick).removeClass('desactivarClick');
				alert("Edite algún campo.",'','', "Cerrar");
			}


		});



		$(t.btnResetPerfil).on('click', function(){
			$.xPromptQ({header: '¿Estas seguro resetear a este usuario?'}, function(i){
		    console.log(i)
				if (i==true){
					$('#btn_close_modal').click();
					console.log($('#multiple_select').val());

				}
		  });

		});


	},


};
