var ModeloResource = function(id, ip, name, description, rank, available, geometry){
  this.id=id;
  this.ip=ip;
  this.name=name.toUpperCase();
  this.description=description;
  this.rank=rank;
  this.available=available;
  this.geometry=geometry;
}

ModeloResource.prototype = {
	constructor: ModeloResource,
  getForID: function(){
      var t = this;
      //console.log(t.email+'/'+t.pass);
      return new Promise(function(resolve, reject) {

      $.ajax({
                       url: '/api/recursos/'+t.id,
                       type: 'GET',
                       dataType: 'json',
                       success: function (data) {
                           resolve(data);
                       },
                       error: function (xhr, textStatus, errorThrown) {
                           reject(textStatus);
                       }
                   });
      });
    },
    getForIP: function(){
        var t = this;
        //console.log(t.email+'/'+t.pass);
        return new Promise(function(resolve, reject) {

        $.ajax({
                         url: '/api/recursosIp/&ip_plug='+t.ip,
                         type: 'GET',
                         dataType: 'json',
                         success: function (data) {
                             resolve(data);
                         },
                         error: function (xhr, textStatus, errorThrown) {
                             reject(textStatus);
                         }
                     });
        });
      },

  get: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/recursos',
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  getHistorial: function(){
    var t = this;
    console.log('modelo resource',t.id);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/resourceHistorial/&resource_id='+t.id,
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  getGeo: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/recursos/geo',
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  delete: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/recursos/'+t.id,
                     type: 'DELETE',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  addResource: function() {
    var t = this;
    var  newResource= {
      "ip_plug": t.ip,
      "name": t.name,
      "description": t.description,
      "rank": t.rank,
      "available": t.available,
      "geometry": t.geometry
      };
      console.log('newResource:   '+ JSON.stringify(newResource));
    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/recursos',
                           type: 'POST',
                           dataType: 'json',
                           data: newResource,
                           success: function (data) {
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });

      },

  update: function() {
    var t = this;
    var  newResource= {
      "ip_plug": t.ip,
      "name": t.name,
      "description": t.description,
      "rank": t.rank,
      "available": t.available,
      "geometry": t.geometry
      };

      console.log('newResource :   '+ JSON.stringify(newResource));
    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/recursos/'+t.id,
                           type: 'PUT',
                           dataType: 'json',
                           data: newResource,
                           success: function (data) {
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });
  },
  apagar: function() {
    var t = this;

    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/apagar/'+t.id,
                           type: 'POST',
                           dataType: 'json',
                           success: function (data) {
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });

      },

}
