var ModeloPlan = function(id, startTime, endTime, resource_id, user_id){
  this.id=id;
  this.startTime=startTime;
  this.endTime=endTime;
  this.resource_id=resource_id;
  this.user_id=user_id;
}

ModeloPlan.prototype = {
	constructor: ModeloPlan,
  getForID: function(){
      var t = this;
      //console.log(t.email+'/'+t.pass);
      return new Promise(function(resolve, reject) {

      $.ajax({
                       url: '/api/planning/'+t.id,
                       type: 'GET',
                       dataType: 'json',
                       success: function (data) {
                           resolve(data);
                       },
                       error: function (xhr, textStatus, errorThrown) {
                           reject(textStatus);
                       }
                   });
      });
    },
  get: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/planning',
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },



  delete: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/planning/'+t.id,
                     type: 'DELETE',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  deleteBefore: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/planning/before/'+t.id,
                     type: 'DELETE',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  addPlan: function() {
    var t = this;
    var  newUso=
    {
      "date" :
      {
        "startTime" : t.startTime,
        "endTime" : t.endTime
      },
      "resource_id" : t.resource_id,
      "user_id" : t.user_id
    };

    console.log('newPlan:   '+ JSON.stringify(newUso));
    console.log('newPlan:   ', newUso);

    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/planning',
                           type: 'POST',
                           dataType: 'json',
                           data: newUso,
                           success: function (data) {
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });

      },

  update: function() {
    var t = this;
    var  newUso=
    {
      "date" :
      {
        "startTime" : t.startTime,
        "endTime" : t.endTime
      },
      "resource_id" : t.resource_id,
      "user_id" : t.user_id
    };

      console.log('newUso :   '+ JSON.stringify(newUso));
    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/planning/'+t.id,
                           type: 'PUT',
                           dataType: 'JSON',
                           data: newUso,
                           success: function (data) {
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });
  },

}
