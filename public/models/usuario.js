var ModeloUser = function(id, name, surname, email, role, rank, available, geometry, extraResources, pass, tag){
  this.id=id;
  this.name=name.toUpperCase();
  this.surname=surname.toUpperCase();
  this.email=email.toUpperCase();
  this.role=role.toUpperCase();
  this.rank=rank;
  this.available=available;
  this.geometry=geometry;
  this.extraResources=extraResources;
  this.pass=pass;
  this.tag=tag;
}
ModeloUser.prototype = {
	constructor: ModeloUser,
  getForID: function(){
      var t = this;
      //console.log(t.email+'/'+t.pass);
      return new Promise(function(resolve, reject) {

      $.ajax({
                       url: '/api/usuarios/'+t.id,
                       type: 'GET',
                       dataType: 'json',
                       success: function (data) {
                           resolve(data);
                       },
                       error: function (xhr, textStatus, errorThrown) {
                           reject(textStatus);
                       }
                   });
      });
    },
  get: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/usuarios',
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  getForCredenciales: function(){
    var t = this;
    console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/credenciales/&email='+t.email,
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  getHistorial: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/userHistorial/&user_id='+t.id,
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  getPlannings: function(){
    var t = this;
    console.log(t);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/plan/&user_id='+t.id,
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  getGeo: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/usuarios/geo',
                     type: 'GET',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  delete: function(){
    var t = this;
    //console.log(t.email+'/'+t.pass);
    return new Promise(function(resolve, reject) {

    $.ajax({
                     url: '/api/usuarios/'+t.id,
                     type: 'DELETE',
                     dataType: 'json',
                     success: function (data) {
                         resolve(data);
                     },
                     error: function (xhr, textStatus, errorThrown) {
                         reject(textStatus);
                     }
                 });
    });
  },

  addUser: function() {
    var t = this;
    var  newUser= {
        "name":  t.name ,
        "surname": t.surname,
        "email": t.email,
        "pass": t.pass,
        "role": t.role,
        "rank": t.rank || 1,
        "available": t.available,
        "geometry": t.geometry,
        "extraResources": t.extraResources,
        "tag": t.tag
      };
      console.log('newuser usuario:   '+ JSON.stringify(newUser));
    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/usuarios',
                           type: 'POST',
                           dataType: 'json',
                           data: newUser,
                           success: function (data) {
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });

      },

  update: function() {
    var t = this;
    var  newUser= {
        "name":  t.name ,
        "surname": t.surname,
        "email": t.email,
        "pass": t.pass,
        "role": t.role,
        "rank": t.rank || 1,
        "available": t.available,
        "geometry": t.geometry,
        "extraResources": t.extraResources,
        "tag": t.tag
      };
      console.log('addUser:   '+t.email+'/'+t.pass+'extraresources: '+t.extraResources);
      console.log('newuser usuario:   ', newUser);
    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/usuarios/'+t.id,
                           type: 'PUT',
                           dataType: 'JSON',
                           data: newUser,
                           success: function (data) {
                             console.log('newuser data:   ', data);
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });
  },
  updateResources: function() {
    var t = this;
    var  newUser= {
        "name":  t.name ,
        "surname": t.surname,
        "email": t.email,
        "pass": t.pass,
        "role": t.role,
        "rank": t.rank || 1,
        "available": t.available,
        "geometry": t.geometry,
        "extraResources": t.extraResources,
        "tag": t.tag
      };
      console.log('addUser:   '+t.email+'/'+t.pass+'extraresources: '+t.extraResources);
      console.log('newuser usuario:   ', newUser);
    return new Promise(function(resolve, reject) {

          $.ajax({
                           url: '/api/useresources/'+t.id,
                           type: 'PUT',
                           dataType: 'JSON',
                           data: newUser,
                           success: function (data) {
                             console.log('newuser data:   ', data);
                               resolve(data);
                           },
                           error: function (xhr, textStatus, errorThrown) {
                               reject(textStatus);
                           }
                       });
          });
  },

};
