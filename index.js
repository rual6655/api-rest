const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var http = require('http')


// set up express app
const app = express();

// connect to mongodb
mongoose.connect('mongodb://localhost/MAmi');
mongoose.Promise = global.Promise;

//set up static files
app.use(express.static('public'));


app.use(bodyParser.urlencoded({
   extended: false
}));

// use body-parser middleware
app.use(bodyParser.json());

// initialize routes
app.use('/api', require('./routes/api'));

// error handling middleware
app.use(function(err, req, res, next){
    console.log(err); // to see properties of message in our console
    res.status(422).send({error: err.message});
});

/*app.listen(3000,'192.168.100.18', function(){
  console.log('now listening for requests');
  console.log('Server started at http://192.168.100.18:3000');
})*/

// listen for requests
app.listen(process.env.port || 3000, function(){
    console.log('now listening for requests');
});


module.exports=app;
