const express = require ('express');
const router = express.Router();
const ObjectId = require('mongodb').ObjectID;
const axios = require ('axios');
var querystring = require('querystring');
const Usuario = require('../models/Schemas/usuario');
const Recurso = require('../models/Schemas/recurso');
const Uso = require('../models/Schemas/uso');
const Planning = require('../models/Schemas/plan');
var exec = require('child_process').exec;

var hr;
var min;
var sec;
var months;
var days;
var curWeekDay;
var curDay;
var curMonth;
var curYear;
var date;
var reloj = hr + ":" + min + ":" + sec + " "+ " - "+ date;
var date_reloj = "";


var inicio= 0;
var final= 0;

var array_recursos= [];

//var for update local planning query

var query_planning = [];

start_query_planning();
start_query_resources_ip();


// get a list of usuario from the db

router.get('/usuarios', function(req, res, next){
    Usuario.find({}).then(function(usuarios){
        res.send(usuarios);

        console.log(usuarios.length);

        for(var i = 0; i < usuarios.length; i++) {
            console.log(usuarios[i]);  // (o el campo que necesites)
        }
    });
});

router.get('/usuarios/geo', function(req, res, next){
    Usuario.geoNear(
        {type: 'Point', coordinates: [parseFloat(req.query.lng), parseFloat(req.query.lat)]},
        {maxDistance: 1000, spherical: true}
        ).then(function(usuarios){
            res.send(usuarios);
        }).catch(next);

    });

router.get('/usuarios/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario);
        });
    }).catch(next);
});

router.get('/usuariosTAG/:id', function(req, res, next){
  var request = querystring.parse(req.url);
  //console.log('este es el usuario a buscar por tag: ------------', request);
  Usuario.find({ tag : request.tag_user }).then(function(usuario){
        res.send(usuario);
  });
});

router.get('/usuarios/name/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.name);
        });
    }).catch(next);
});

router.get('/usuarios/surname/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.surname);
        });
    }).catch(next);
});

router.get('/usuarios/email/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.email);
        });
    }).catch(next);
});

router.get('/usuarios/role/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.role);
        });
    }).catch(next);
});

router.get('/usuarios/rank/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.rank);
        });
    }).catch(next);
});

router.get('/usuarios/available/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.available);
        });
    }).catch(next);
});

router.get('/usuarios/geometry/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.geometry.coordinates);
        });
    }).catch(next);
});

router.get('/usuarios/extraResources/:id', function(req, res, next){
    Usuario.findById({_id: req.params.id}, req.body).then(function(){
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario.extraResources);
        });
    }).catch(next);
});


// add a new usuario to the db
router.post('/usuarios', function(req, res, next){
    Usuario.create(req.body).then(function(usuario){
        res.send(usuario);
    }).catch(next);
});

// update a usuario in the db
router.put('/usuarios/:id', function(req, res, next){
    Usuario.findByIdAndUpdate({_id: req.params.id}, req.body).then(function(){
      console.log('este es el usuario a actualizar', req.body)
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario);
        });
    }).catch(next);
});

router.put('/useresources/:id', function(req, res, next){
    Usuario.findByIdAndUpdate({_id: req.params.id}, { "$push": { "extraResources": req.body.extraResources } },
    { "new": true, "upsert": true }).then(function(){
      console.log('este es el usuario a actualizar', req.body)
        Usuario.findOne({_id: req.params.id}).then(function(usuario){
            res.send(usuario);
        });
    }).catch(next);
});
// delete a usuario from the db
router.delete('/usuarios/:id', function(req, res, next){
    Usuario.findByIdAndRemove({_id: req.params.id}).then(function(usuario){
        res.send(usuario);
    }).catch(next);
});



//################################################################################


// get a list of recurso from the db
router.get('/recursos', function(req, res, next){
    Recurso.find({}).then(function(recursos){
        res.send(recursos);
    });

});

router.get('/recursos/geo', function(req, res, next){
    Recurso.geoNear(
        {type: 'Point', coordinates: [parseFloat(req.query.lng), parseFloat(req.query.lat)]},
        {maxDistance: 100000, spherical: true}
        ).then(function(recurso){
            res.send(recurso);
        }).catch(next);

    });

router.get('/recursos/:id', function(req, res, next){
    Recurso.findById({_id: req.params.id}, req.body).then(function(){
        Recurso.findOne({_id: req.params.id}).then(function(recurso){
            res.send(recurso);
        });
    }).catch(next);
});

router.get('/recursosIp/:id', function(req, res, next){
  var request = querystring.parse(req.url);
  //console.log('este es el recurso a buscar por ip: ------------', request);
  Recurso.find({ ip_plug : request.ip_plug }).then(function(recurso){
        res.send(recurso);
  });
});

router.get('/recursos/name/:id', function(req, res, next){
    Recurso.findById({_id: req.params.id}, req.body).then(function(){
        Recurso.findOne({_id: req.params.id}).then(function(recurso){
            res.send(recurso.name);
        });
    }).catch(next);
});


router.get('/recursos/description/:id', function(req, res, next){
    Recurso.findById({_id: req.params.id}, req.body).then(function(){
        Recurso.findOne({_id: req.params.id}).then(function(recurso){
            res.send(recurso.description);
        });
    }).catch(next);
});

router.get('/recursos/rank/:id', function(req, res, next){
    Recurso.findById({_id: req.params.id}, req.body).then(function(){
        Recurso.findOne({_id: req.params.id}).then(function(recurso){
            res.send(recurso.rank);
        });
    }).catch(next);
});

router.get('/recursos/available/:id', function(req, res, next){
    Recurso.findById({_id: req.params.id}, req.body).then(function(){
        Recurso.findOne({_id: req.params.id}).then(function(recurso){
            res.send(recurso.available);
        });
    }).catch(next);
});

router.get('/recursos/geometry/:id', function(req, res, next){
    Recurso.findById({_id: req.params.id}, req.body).then(function(){
        Recurso.findOne({_id: req.params.id}).then(function(recurso){
            res.send(recurso.geometry.coordinates);
        });
    }).catch(next);
});

// add a new recurso to the db
router.post('/recursos', function(req, res, next){
    Recurso.create(req.body).then(function(recurso){
        res.send(recurso);
        start_query_resources_ip();
    }).catch(next);
});

// update a recurso in the db
router.put('/recursos/:id', function(req, res, next){
    Recurso.findByIdAndUpdate({_id: req.params.id}, req.body).then(function(){
        Recurso.findOne({_id: req.params.id}).then(function(recurso){
            res.send(recurso);
            start_query_resources_ip();
        });
    }).catch(next);
});



// delete a recurso from the db
router.delete('/recursos/:id', function(req, res, next){
    Recurso.findByIdAndRemove({_id: req.params.id}).then(function(recurso){
        res.send(recurso);
        start_query_resources_ip();
    }).catch(next);
});

//################################################################################


// get a list of Uso from the db
router.get('/uso', function(req, res, next){

    Uso.find({}).then(function(Uso){
        res.send(Uso);
    });

});


router.get('/uso/:id', function(req, res, next){
    Uso.findById({_id: req.params.id}, req.body).then(function(){
        Uso.findOne({_id: req.params.id}).then(function(Uso){
            res.send(Uso);
        });
    }).catch(next);
});

router.get('/uso/resource_id/:id', function(req, res, next){
    Uso.findById({_id: req.params.id}, req.body).then(function(){
        Uso.findOne({_id: req.params.id}).then(function(Uso){
            res.send(Uso.resource_id);
        });
    }).catch(next);
});

router.get('/uso/user_id/:id', function(req, res, next){
    Uso.findById({_id: req.params.id}, req.body).then(function(){
        Uso.findOne({_id: req.params.id}).then(function(Uso){
            res.send(Uso.user_id);
        });
    }).catch(next);
});

router.get('/uso/date/:id', function(req, res, next){
    Uso.findById({_id: req.params.id}, req.body).then(function(){
        Uso.findOne({_id: req.params.id}).then(function(Uso){
            res.send(Uso.date);
        });
    }).catch(next);
});

router.get('/uso/date/starTime/:id', function(req, res, next){
    Uso.findById({_id: req.params.id}, req.body).then(function(){
        Uso.findOne({_id: req.params.id}).then(function(Uso){
            res.send(Uso.date.starTime);
        });
    }).catch(next);
});

router.get('/uso/date/endTime/:id', function(req, res, next){
    Uso.findById({_id: req.params.id}, req.body).then(function(){
        Uso.findOne({_id: req.params.id}).then(function(Uso){
            res.send(Uso.date.endTime);
        });
    }).catch(next);
});

// add a new Uso to the db
router.post('/uso', function(req, res, next){
    Uso.create(req.body).then(function(Uso){
        res.send(Uso);
    }).catch(next);
});

// update a Uso in the db
router.put('/uso/:id', function(req, res, next){
    Uso.findByIdAndUpdate({_id: req.params.id}, req.body).then(function(){
        Uso.findOne({_id: req.params.id}).then(function(Uso){
            res.send(Uso);
        });
    }).catch(next);
});


// delete a Uso from the db
router.delete('/uso/:id', function(req, res, next){
    Uso.findByIdAndRemove({_id: req.params.id}).then(function(Uso){
        res.send(Uso);
    }).catch(next);
});

//################################################################################


// get a list of planning from the db
router.get('/planning', function(req, res, next){

    Planning.find({}).then(function(planning){
        res.send(planning);
    });

});


router.get('/planning/:id', function(req, res, next){
    Planning.findById({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
            res.send(planning);
        });
    }).catch(next);
});

router.get('/planning/date/:id', function(req, res, next){
    Planning.findById({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
            res.send(planning.date);
        });
    }).catch(next);
});

router.get('/planning/date/starTime/:id', function(req, res, next){
    Planning.findById({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
            res.send(planning.date.starTime);
        });
    }).catch(next);
});

router.get('/planning/date/endTime/:id', function(req, res, next){
    Planning.findById({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
            res.send(planning.date.endTime);
        });
    }).catch(next);
});

router.get('/planning/resource_id/:id', function(req, res, next){
    Planning.findById({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
            res.send(planning.resource_id);
        });
    }).catch(next);
});

router.get('/planning/user_id/:id', function(req, res, next){
    Planning.findById({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
            res.send(planning.user_id);
        });
    }).catch(next);
});
router.get('/plan/:id', function(req, res, next){
  var request = querystring.parse(req.url);
//console.log(request);
//console.log(request.resource_id);
//console.log(request.user_id);
  Planning.find({ user_id : request.user_id }).then(function(planning){
        res.send(planning);
  });
});

// add a new planning to the db
router.post('/planning', async function(req, res, next){
    var extraResourceIs = false;
    var nearUser = await getUserID(req.body.user_id);
    var nearResource = await getResourceID(req.body.resource_id);
    var disponible_fecha = false;
    console.log('request-----',req.body);
    console.log(nearUser.data.extraResources);
    console.log('dispositivo listo---',nearResource.data.available);

      esta_resource_usuario(extraResourceIs, nearUser, nearResource).then(function(resolve,reject){
        console.log('esta_resource_usuario', resolve);
        extraResourceIs = resolve;
        if (extraResourceIs == true){
          buscar_query_planning(req.body.resource_id, req.body['date[startTime]'], req.body['date[endTime]']).then(async function(resolve,reject){
          console.log('extraResourceIs -> disponible',resolve);
          disponible_fecha = resolve;
          if (disponible_fecha){
              var data = {
                "date": {
                  "startTime": req.body['date[startTime]'],
                  "endTime": req.body['date[endTime]'],
                },
                "resource_id": nearResource.data._id,
                "user_id": nearUser.data._id
                }

              try{
                await axios.post('http://localhost:3000/api/planningadd', data);
                res.send(data);
              }catch(error){
                 console.log(error);
                 console.log('error al añadir plan ---' +error);
              }
          }else{
            res.send('El recurso no está disponible para esa fecha');
          }
          });
        }else{
          if (nearUser.data.rank >= nearResource.data.rank){
            buscar_query_planning(req.body.resource_id, req.body['date[startTime]'], req.body['date[endTime]']).then(async function(resolve,reject){
            console.log('no extraResourceIs -> disponible', resolve);
            disponible_fecha = resolve;
            if (disponible_fecha){
                var data = {
                  "date": {
                    "startTime": req.body['date[startTime]'],
                    "endTime": req.body['date[endTime]'],
                  },
                  "resource_id": nearResource.data._id,
                  "user_id": nearUser.data._id
                  }

                try{
                  await axios.post('http://localhost:3000/api/planningadd', data);
                  res.send(data);
                }catch(error){
                   console.log(error);
                   console.log('error al añadir plan ---' +error);
                }
            }else{
              res.send('El recurso no está disponible para esa fecha');
            }
            });
          }else{
            res.send('No tienes suficiente rango para usar este recurso');
          }

        }
      });

});

router.post('/rfid/:id', async function(req, res, next){
  var request = querystring.parse(req.url);
  console.log('tag del usuario', request.tag);
  var userTAG= await findUserForTag(request.tag);
  console.log('el usuario es', userTAG.data[0]._id);
  var resourceIP= await findResourceForIP(request.resource_ip);
  console.log('el recurso es', resourceIP.data[0]._id);

  Planning.find({ resource_id : resourceIP.data[0]._id,  user_id : userTAG.data[0]._id }).then( async function(planning){
    if (planning.length!=0){
      for(var i=0; i<planning.length; i++){


      var inicioRFID = new Date(date_reloj) - new Date (planning[i].date.startTime);
      var finalRFID = new Date(date_reloj) - new Date (planning[i].date.endTime);
      console.log(inicioRFID);
      console.log(finalRFID);
      console.log(new Date(date_reloj))
      console.log(new Date (planning[i].date.startTime))
      console.log(new Date (planning[i].date.endTime))



      if (finalRFID >= 0){
         try{

         }catch(error){
            console.log(error);
            console.log('borrar plan --- '+error);
         }
       } else if (inicioRFID >= 0 && finalRFID <0 ) {
        /*if(planning[0].available == false){

        }else{*/
           var ip_re = get_ip_recurso(planning[i].resource_id);
           encender_recurso(ip_re);
          //planning[0].available = false;
        //}
       }else if (inicioRFID < 0 && finalRFID <0){

       }
     }
  }
        res.send(planning);
  });
});

router.post('/rfidall/:id', async function(req, res, next){
  var request = querystring.parse(req.url);
  console.log('tag del usuario', request.tag);
  var userTAG= await findUserForTag(request.tag);
  console.log('el usuario es', userTAG.data[0]._id);

  Planning.find({ user_id : userTAG.data[0]._id }).then( async function(planning){
    if (planning.length!=0){
      for(var i=0; i<planning.length; i++){


      var inicioRFID = new Date(date_reloj) - new Date (planning[i].date.startTime);
      var finalRFID = new Date(date_reloj) - new Date (planning[i].date.endTime);
      console.log(inicioRFID);
      console.log(finalRFID);
      console.log(new Date(date_reloj))
      console.log(new Date (planning[i].date.startTime))
      console.log(new Date (planning[i].date.endTime))



      if (finalRFID >= 0){
         try{

         }catch(error){
            console.log(error);
            console.log('borrar plan --- '+error);
         }
       } else if (inicioRFID >= 0 && finalRFID <0 ) {
        /*if(planning[0].available == false){

        }else{*/
           var ip_re = get_ip_recurso(planning[i].resource_id);
           encender_recurso(ip_re);
          //planning[0].available = false;
        //}
       }else if (inicioRFID < 0 && finalRFID <0){

       }
     }
  }
        res.send(planning);
  });
});

async function findUserForTag(tag){
  try{
    return await axios.get('http://localhost:3000/api/usuariosTAG/&tag_user='+tag);
  }catch(error){
     console.log(error);
     console.log('error get recursos ---' +error);
  }

}

async function findResourceForIP(ipResource){
  try{
    return await axios.get('http://localhost:3000/api/recursosIp/&ip_plug='+ipResource);
  }catch(error){
     console.log(error);
     console.log('error get recursos ---' +error);
  }
}

router.post('/planningaddrfid', async function(req, res, next){
  var startTime = curYear+'-'+uncurMonth+'-'+checkTime(curDay)+'T'+checkTime(hr)+':'+min+':'+sec+'.000Z';;
  var endTime = curYear+'-'+uncurMonth+'-'+checkTime(curDay)+'T'+checkTime(hr+1)+':'+min+':'+sec+'.000Z';;
  var extraResourceIs = false;
  var nearUser = await getUserID(req.body.user_id);
  var nearResource = await getResourceID(req.body.resource_id);
  var disponible_fecha = false;
  console.log('request-----',req.body);
  console.log(nearUser.data.extraResources);
  console.log('dispositivo listo---',nearResource.data.available);
  if (nearResource.data.available){
    esta_resource_usuario(extraResourceIs, nearUser, nearResource).then(function(resolve,reject){
      console.log('esta_resource_usuario', resolve);
      extraResourceIs = resolve;
      if (extraResourceIs == true){
        buscar_query_planning(req.body.resource_id, startTime, endTime).then(async function(resolve,reject){
        console.log('extraResourceIs -> disponible',resolve);
        disponible_fecha = resolve;
        if (disponible_fecha){

            var data = {
              "date": {
                "startTime": startTime,
                "endTime": endTime,
              },
              "resource_id": nearResource.data._id,
              "user_id": nearUser.data._id
              }

            try{
              await axios.post('http://localhost:3000/api/planningadd', data);
              res.send(data);
            }catch(error){
               console.log(error);
               console.log('error al añadir plan ---' +error);
            }

        }else{
          res.send('El recurso no está disponible para esa fecha');
        }
        });
      }else{
        if (nearUser.data.rank >= nearResource.data.rank){
          buscar_query_planning(req.body.resource_id, startTime, endTime).then(async function(resolve,reject){
          console.log('no extraResourceIs -> disponible', resolve);
          disponible_fecha = resolve;
          if (disponible_fecha){

            var data = {
              "date": {
                "startTime": startTime,
                "endTime": endTime,
              },
              "resource_id": nearResource.data._id,
              "user_id": nearUser.data._id
              }

              try{
                await axios.post('http://localhost:3000/api/planningadd', data);
                res.send(data);
              }catch(error){
                 console.log(error);
                 console.log('error al añadir plan ---' +error);
              }

          }else{
            console.log('El recurso no está disponible para esa fecha');
            res.send('El recurso no está disponible para esa fecha');
          }
          });
        }else{
          console.log('No tienes suficiente rango para usar este recurso');
          res.send('No tienes suficiente rango para usar este recurso');
        }

      }
    });
  }else{
    console.log('El recurso no está disponible ahora mismo');
    res.send('El recurso no está disponible ahora mismo');
  }
});

router.post('/planningadd', function(req, res, next){
    Planning.create(req.body).then(function(planning){
        res.send(planning);
        update_query_planning(planning, 'add');
        add_uso(planning);
    }).catch(next);
});

// update a planning in the db
router.put('/planning/:id', function(req, res, next){
    Planning.findByIdAndUpdate({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
            res.send(planning);
            update_query_planning(planning, 'update');
        });
    }).catch(next);
});

/*router.put('/planning/before/:id', function(req, res, next){
    Planning.findByIdAndUpdate({_id: req.params.id}, req.body).then(function(){
        Planning.findOne({_id: req.params.id}).then(function(planning){
          update_query_planning(planning, 'update');
          update_uso_before(planning);
          res.send(planning);
        });
    }).catch(next);
});*/


// delete a planning from the db
router.delete('/planning/:id', function(req, res, next){
    Planning.findByIdAndRemove({_id: req.params.id}).then(function(planning){
      update_query_planning(planning, 'delete');
      res.send(planning);
    }).catch(next);
});

router.delete('/planning/before/:id', function(req, res, next){
    Planning.findByIdAndRemove({_id: req.params.id}).then(function(planning){
      update_query_planning(planning, 'delete');
      delete_uso_before(planning);
      res.send(planning);
    }).catch(next);
});


router.get('/credenciales/:id', function(req, res, next){
  var request = querystring.parse(req.url);
//console.log(request);
//console.log(request.resource_id);

//console.log(request.user_id);
  Usuario.find({ email : request.email }).then(function(user){
        res.send(user);
  });
});


//consigue el uso de ese recurso para ese usuario. id es la concatenación de los id del recurso y del usuario
router.get('/usoRecurso/:id', function(req, res, next){
  var request = querystring.parse(req.url);
//console.log(request);
//console.log(request.resource_id);
//console.log(request.user_id);
  Uso.find({ resource_id : request.resource_id,  user_id : request.user_id }).then(function(uso){
        res.send(uso);
  });
});

router.get('/resourceHistorial/:id', function(req, res, next){
  var request = querystring.parse(req.url);
//console.log(request);
//console.log(request.resource_id);
//console.log(request.user_id);
  Uso.find({ resource_id : request.resource_id }).then(function(uso){
        res.send(uso);
  });
});

router.get('/userHistorial/:id', function(req, res, next){
  var request = querystring.parse(req.url);
//console.log(request);
//console.log(request.resource_id);
//console.log(request.user_id);
  Uso.find({ user_id : request.user_id }).then(function(uso){
        res.send(uso);
  });
});

//consigue el plan de ese recurso para ese usuario. id es la concatenación de los id del recurso y del usuario
router.get('/planRecurso/:id', function(req, res, next){
  var request = querystring.parse(req.url);
  Planning.find({ resource_id : request.resource_id,  user_id : request.user_id }).then(function(planning){
        res.send(planning);
  });
});

router.post('/apagar/:id', function(req, res, next){
  var request = querystring.parse(req.url);
  for(var i= 0; i< array_recursos.length; i++){
    if (request.resource_id == array_recursos.id){
      apagar_recurso(array_recursos[i].ip);
    }
  }
});

async function start_query_resources_ip(){
  var response_resource = await get_resources();

  var json_response_resource = response_resource.data;
  //var json_response_stringify = JSON.stringify(object_response.data);

  for (var i=0; i<json_response_resource.length; i++){
    array_recursos[i] = {
      id : json_response_resource[i]._id,
      ip: json_response_resource[i].ip_plug,
      available: json_response_resource[i].available,
      rank: json_response_resource[i].rank,
      description: json_response_resource[i].description,
      name: json_response_resource[i].name,
      geo: json_response_resource[i].geometry
    };
  }
  //console.log(query_planning[0]._id);

  //console.log('inicializado array de resources --- ok');

  //console.log(array_recursos);
}

async function get_resources(){
  try{
    return await axios.get('http://localhost:3000/api/recursos');
  }catch(error){
     console.log(error);
     console.log('error get recursos ---' +error);
  }
}

async function add_uso(planning){
  var data = {
    "date": {
      "startTime": planning.date.startTime,
      "endTime": planning.date.endTime,
    },
    "resource_id": planning.resource_id,
    "user_id": planning.user_id
    }

  try{
    return await axios.post('http://localhost:3000/api/uso', data);
  }catch(error){
     console.log(error);
     console.log('error al añadir uso ---' +error);
  }
}

async function delete_uso_before(planning){
  var uso_recurso_user_object = await get_use_for_resource_user(planning);
  //console.log('uso_recurso_user_object------------',uso_recurso_user_object);
  var uso_recurso_user = uso_recurso_user_object.data;
  //console.log('uso_recurso_user------------',uso_recurso_user);

  var uso_recurso_user_to_delete= '';
  for (var i=0; i< uso_recurso_user.length; i++){
    var time_uso = new Date(planning.date.startTime) - new Date(uso_recurso_user[i].date.startTime);
    if (time_uso == 0){
      //console.log('entro aqui---------------------');
      uso_recurso_user_to_delete = uso_recurso_user[i];
    }
  }
  console.log('A borrar este uso: ----', uso_recurso_user_to_delete);
  try{
    return await axios.delete('http://localhost:3000/api/uso/'+uso_recurso_user_to_delete._id);
  }catch(error){
     console.log(error);
     console.log('error encontrando el uso de del recurso para ese usuario ---' +error);
  }
};

/*async function update_uso_before(planning){
  var uso_recurso_user_object = await get_use_for_resource_user(planning);
  //console.log('uso_recurso_user_object------------',uso_recurso_user_object);
  var uso_recurso_user = uso_recurso_user_object.data;
  //console.log('uso_recurso_user------------',uso_recurso_user);

  var uso_recurso_user_to_update= '';
  for (var i=0; i< uso_recurso_user.length; i++){
    var time_uso = new Date(planning.date.startTime) - new Date(uso_recurso_user[i].date.startTime);
    if (time_uso == 0){
      //console.log('entro aqui---------------------');
      uso_recurso_user_to_delete = uso_recurso_user[i];
    }
  }
  console.log('A actualizar este uso: ----', uso_recurso_user_to_update);
  try{
    return await axios.put('http://localhost:3000/api/uso/'+uso_recurso_user_to_update._id);
  }catch(error){
     console.log(error);
     console.log('error encontrando el uso de del recurso para ese usuario ---' +error);
  }
};*/

async function get_use_for_resource_user(planning){
  //console.log('get_use_for_resource_user-----------',planning._id);
  try{
    return await axios.get('http://localhost:3000/api/usoRecurso/&resource_id='+planning.resource_id+'&user_id='+planning.user_id);
  }catch(error){
     console.log(error);
     console.log('error encontrando el uso de del recurso para ese usuario --- '+error);
  }
};

async function start_query_planning(){
  var object_response = await getPlan();
  var json_response = object_response.data;
  //var json_response_stringify = JSON.stringify(object_response.data);

  for (var i=0; i<json_response.length; i++){
    query_planning[i] = json_response[i];
  }
  //console.log(query_planning[0]._id);

  //console.log('inicializado query de planes --- ok');

  //console.log(query_planning);
  order_planning();
};

async function getPlan(){
  try{
    return await axios.get('http://localhost:3000/api/planning')
    //return await axios.get('http://192.168.100.18:3000/api/planning')
  }catch(error){
     console.log(error);
     console.log('inicializado query de planes --- '+error);
  }
};
async function getUserID(id){
  try{
    return await axios.get('http://localhost:3000/api/usuarios/'+id)
    //return await axios.get('http://192.168.100.18:3000/api/planning')
  }catch(error){
     console.log(error);
     console.log('inicializado query de planes --- '+error);
  }
};
async function getResourceID(id){
  try{
    return await axios.get('http://localhost:3000/api/recursos/'+id)
    //return await axios.get('http://192.168.100.18:3000/api/planning')
  }catch(error){
     console.log(error);
     console.log('inicializado query de planes --- '+error);
  }
};

function update_query_planning(planning, petition) {
    console.log('este es el plan a '+petition , planning);

    console.log('actualizando query de planes...');
    if (petition == 'add'){
        //add planning
        query_planning.push(planning);
        console.log('actulizado --- OK');
        order_planning();
    }
    else if(petition == 'update'){
      for (var i=0; i< query_planning.length-1; i++){
        //console.log('id de query',query_planning[i]._id);
        //console.log('id de planing',planning._id);

        if (query_planning[i]._id == planning._id){
          //console.log('aqui esntro');
          query_planning.splice(i,1);
        }
      }
      query_planning.push(planning);
      console.log('actulizado --- OK');
      order_planning();
    }else if (petition == 'delete'){
      /*for (var i=0; i< query_planning.length-1; i++){
        if (query_planning[i]._id == planning._id){
          query_planning.splice(i,1);
        }
      }*/
      query_planning = [];
      start_query_planning();
      console.log('actulizado --- OK');
      order_planning();
    }else {
      console.log('fallo al actualizar --- ERROR');
    }
    //console.log(query_planning);

};

function buscar_query_planning(id_recurso, inicio, final) {
  return new Promise(function(resolve, reject) {
    var boolean = true;
    var i=0;
  if (query_planning.length > 0){
    console.log('query_planning', query_planning);
    while(boolean == true && i <= query_planning.length-1){

      console.log('id_recurso', id_recurso);
      console.log('query_planning.resource_id', query_planning[i].resource_id);
      if (id_recurso == query_planning[i].resource_id){

        inicioPlan = new Date (inicio) - new Date (query_planning[i].date.startTime);
        durantePlan = new Date (inicio) - new Date (query_planning[i].date.endTime);
        finalPlan = new Date (final) - new Date (query_planning[i].date.endTime);
        duranteFinPlan = new Date (final) - new Date (query_planning[i].date.startTime);
        console.log('inicioPlan', inicioPlan);
        console.log('finalPlan', finalPlan);
        console.log('durantePlan', durantePlan);
        console.log('duranteFinPlan', duranteFinPlan);

        if (inicioPlan < 0 && durantePlan < 0 && final <= 0 && duranteFinPlan > 0){
          console.log('1');
          boolean=false;
        }else if (inicioPlan <= 0 && durantePlan < 0 && finalPlan > 0 && duranteFinPlan < 0){
          console.log('2');
          boolean=false;
        }else if (inicioPlan <= 0 && durantePlan < 0 && finalPlan >= 0 && duranteFinPlan > 0){
          console.log('3');
          boolean=false;
        }else if (inicioPlan > 0 && durantePlan < 0 && finalPlan < 0 && duranteFinPlan > 0){
          console.log('4');
          boolean=false;
        }else if (inicioPlan < 0 && durantePlan < 0 && finalPlan < 0 && duranteFinPlan < 0){
          console.log('5');
          boolean=true;
        }else if (inicioPlan > 0 && durantePlan > 0 && finalPlan > 0 && duranteFinPlan > 0){
          console.log('6');
          boolean=true;
        }else if(inicioPlan > 0 && durantePlan < 0 && finalPlan > 0 && duranteFinPlan > 0){
          console.log('7');
          boolean =false;
        }else{
          console.log('8');
          boolean =false;
        }
      }else{
        console.log('9');
        boolean = true;
      }

    i++;
    }
    console.log('boolean', boolean);
    resolve(boolean);
  }else{
    resolve(true);
  }
});
};
function esta_resource_usuario(extraResourceIs, nearUser, nearResource){
  var i = 0;
  return new Promise(function(resolve, reject) {
    console.log(extraResourceIs)
    console.log(nearUser.data.extraResources.length)
  while (extraResourceIs == false &&  i <= nearUser.data.extraResources.length-1){
    console.log('nearUser.data.extraResources[i]', nearUser.data.extraResources[i]);
    console.log('nearResource.data.ip_plug', nearResource.data.ip_plug);
    if (nearUser.data.extraResources[i] == nearResource.data.ip_plug){
      console.log('entro aqui buscando un recurso que contenga el usuario');
      extraResourceIs= true;
      resolve(true);
    }
    i++;
  }
  resolve(false);
});
};

startTime24Hour();

async function startTime24Hour() {
    var today = new Date();
    hr = today.getHours();
    min = today.getMinutes();
    sec = today.getSeconds();
    //Add a zero in front of numbers<10
    min = checkTime(min);
    sec = checkTime(sec);
    //onsole.log(hr + ":" + min + ":" + sec + " ");

    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    curWeekDay = days[today.getDay()];
    curDay = today.getDate();
    curMonth = months[today.getMonth()];
    uncurMonth = checkTime(today.getMonth()+1);
    curYear = today.getFullYear();
    date = curWeekDay+", "+curDay+" "+curMonth+" "+curYear;
    //console.log(date);

    var time = setTimeout(async function(){

      await startTime24Hour();

      date_reloj= curYear+'-'+uncurMonth+'-'+checkTime(curDay)+'T'+checkTime(hr)+':'+min+':'+sec+'.000Z';



      //console.log('query_planning-----', query_planning.length);

      //console.log('esta es la fecha en formato date: '+ date_reloj);


 if (query_planning.length > 0){
      query_planning.forEach(async function(i) {
        //console.log('hora de inicio plan: ',i.date.startTime);
        //console.log('hora final plan: ',i.date.endTime);


        inicio = new Date(date_reloj) - new Date (i.date.startTime);
        final = new Date(date_reloj) - new Date (i.date.endTime);

        //console.log('inicio count:', inicio);
        //console.log('final count:', final);



         if (final >= 0){
             try{
               //if(i.available == true){
                 await axios.delete('http://localhost:3000/api/planning/'+i._id);
                 var ip_re = get_ip_recurso(i.resource_id);
                 apagar_recurso(ip_re);
            //  }else{

               //}

               //return await axios.get('http://192.168.100.18:3000/api/planning')
             }catch(error){
                console.log(error);
                console.log('borrar plan --- '+error);
             }
           } else if (inicio >= 0 && final <0 ) {
            /*if(i.available == false){

            }else{
               var ip_re = get_ip_recurso(i.resource_id);
               encender_recurso(ip_re);
              i.available = false;
            }*/
           }else if (inicio < 0 && final <0){

           }


});
}

     }, 1000);
};

function order_planning(){
    console.log('ordenando planificaciones ------ ');
    query_planning.sort(function(a,b){

      return new Date(b.date.startTime) - new Date(a.date.startTime);
    });

    //console.log('este es el query ordenado',query_planning);
    console.log('ordenando planificaciones ------ ok');
};



/*startTime12Hour();

function startTime12Hour() {
    var today = new Date();
    hr = today.getHours();
    min = today.getMinutes();
    sec = today.getSeconds();
    ap = (hr < 12) ? "AM" : "PM";
    hr = (hr == 0) ? 12 : hr;
    hr = (hr > 12) ? hr - 12 : hr;
    //Add a zero in front of numbers<10
    hr = checkTime(hr);
    min = checkTime(min);
    sec = checkTime(sec);
    console.log(hr + ":" + min + ":" + sec + " " + ap);

    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    curWeekDay = days[today.getDay()];
    curDay = today.getDate();
    curMonth = months[today.getMonth()];
    curYear = today.getFullYear();
    date = curWeekDay+", "+curDay+" "+curMonth+" "+curYear;
    console.log(date);

    var time = setTimeout(function(){ startTime12Hour() }, 500);
}*/

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
};
/*encontrar_recursos();

function  encontrar_recursos(){



    for (var ip=1; ip<= 255; ip++){
    //console.log('192.168.100.'+ip);
    var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t 192.168.100.'+ip+' -c info'
    exec(cmd, function(error, stdout, stderr){
      if(stdout == ''){

      }else{
          console.log(stdout);
          array_recursos.push(stdout);
      }
        //console.log(error);
        //console.log(stderr);
    });
  }
  setTimeout(function(){ console.log('este es el numero de dispositivos en la red: '+array_recursos.length);
  var hola= JSON.parse(array_recursos[0]);
      console.log(hola.system.get_sysinfo.alias);
 }, 5000);
}*/

function get_ip_recurso(id){
  var ip_plug_get= '';
  array_recursos.forEach(function(i) {
    if (id == i.id){
      //console.log('id de redurso a borrar--------', id);
      //console.log('id de redurso a borrar 2--------  ', i.id);
      ip_plug_get= i;
    }
  });
  return ip_plug_get;
};

function encender_recurso(ip){
  var t =ip;
    var data = {
      "ip_plug": t.ip,
      "name": t.name,
      "description": t.description,
      "rank": t.rank,
      "available": false,
      "geometry": t.geo
    }

    //var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t 192.168.100.135 -c off';
    var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t '+ip.ip+' -c on';

    exec(cmd, function(error, stdout, stderr){
        console.log(stdout);
        console.log(error);
        console.log(stderr);
    });

    try{
      return axios.put('http://localhost:3000/api/recursos/'+t.id, data);
    }catch(error){
       console.log(error);
       console.log('error al añadir uso ---' +error);
    }



};

function apagar_recurso(ip){
var t =ip;
  var data = {
    "ip_plug": t.ip,
    "name": t.name,
    "description": t.description,
    "rank": t.rank,
    "available": true,
    "geometry": t.geo
  }

  var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t '+ip.ip+' -c off';

  exec(cmd, function(error, stdout, stderr){
      console.log(stdout);
      console.log(error);
      console.log(stderr);
  });

  try{
    return axios.put('http://localhost:3000/api/recursos/'+t.id, data);
  }catch(error){
     console.log(error);
     console.log('error al añadir uso ---' +error);
  }


};

function estado_recurso(ip){
  var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t '+ip.ip+' -c info';

  exec(cmd, function(error, stdout, stderr){
      console.log(stdout);
      console.log(error);
      console.log(stderr);
  });
};

function energia_recurso(ip){
  var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t '+ip.ip+' -c energy';

  exec(cmd, function(error, stdout, stderr){
      console.log(stdout);
      console.log(error);
      console.log(stderr);
  });
};

function tiempo_recurso(ip){
  var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t '+ip.ip+' -c time';

  exec(cmd, function(error, stdout, stderr){
      console.log(stdout);
      console.log(error);
      console.log(stderr);
  });
};

function info_recurso(ip){
  var cmd = 'cd ./libs/tplink-smartplug-master & python tplink_smartplug.py -t '+ip.ip+' -c info';

  exec(cmd, function(error, stdout, stderr){
      console.log(stdout);
      console.log(error);
      console.log(stderr);
  });
};




module.exports = router;
