# rest-api


# How to use
Requisitos previos.

Hardware:
Un dispositivo con Windows 10 64 bits.
Un dispositivo IOS o Android con el que poder descargar la app de TP-Link Kasa .Smart.
n número de enchufes inteligentes TP-Link Smart WI-FI Plug HS100 o HS110.
n número de lectores RFID.
n número de raspberry pi.
x tarjetas RFID.


Software:
Windows 10 64 bits.
Instalado Node.js (versión 10.15.3 o posterior).
Instalado Python y configurado el PATH para la ejecución de python en consola (versión 2.7.8, con posteriores no funciona)
Instalado MongoDB (para iniciar la base de datos llamada por defecto MAmi en localhost. Si no, cambiar la dirección de la BBDD en el fichero index.js de la raiz del proyecto y las referencias en el fichero routes/api.js a localhost) y Studio 3T (MongoDB Compass Community).
Una consola del sistema como Cmder o ConEmu (también vale la propia de Windows)
Advanced IP Scanner 2.5 o cualquier o cualquier software que permita el escaneo de la red para encontrar las IPs de los dispositivos conectados.
Navegador de Internet como Google Chrome o similares.
App ios o Android Kasa Smart.

Instrucciones de instalación y puesta en marcha.

Instalación y arranque.

En nuestro dispositivo donde deseamos ejecutarlo:
Descargar el proyecto ActiveSpace en el directorio de Windows que se desee.
Crear el directorio C:\data\db (debe de estar creado en el directorio de instalación de mongoDB)
Ejecutar mongod.exe (SI NO ARRANCA, ES PORQUE EN EL PASO ANTERIOR NO HAS CREADO LAS DOS CARPETAS EN EL MISMO DRECTORIO DE INSATLACIÓN DE MONGODB)
Abrir la consola del sistema instalada o la de windows
Ejecutar estos comandos: cd C:\Users\rual6\Documents\proyectosReact\api-rest\master (o en el directorio de descarga)
                         npm start
Y esperar que no ocurra ningún error de ejecución
Abrir nuestro navegador y poner la dirección URL http://localhost:3000 o http://ip_del_dispositivo_donde_se_ejecuta:3000 para entrar en la plataforma web ActiveSpace (solo es la primera vez, después con poner ActiveSpace ya te lo da de sugerencia el navegador).

Registrate, si aun no lo has hecho, e iniciar sesión (para añadir un admin edita en tu base de datos local un usuario ya registrado cambiando el parámetro role = "ADMIN")

Ahora desde ese admin puedes añadir más usurios o recursos y gestionarlos.

Preparación del hardware.
Enchufa los n enchufes HS100 o HS110 en los lugares correspondientes.
Inicia la app Kasa Smart y, configura y conecta a la misma red Wi-Fi del dispositivo que ejecuta el proyecto los enchufes (muy importante no actualizar el firmware de los enchufes, ya que pueden dejar de funcionar con ActiveSpace).
Ejecutar el programa de escaneo de ip instalado y localizar las IPs de los enchufes.
Con una cuenta de administrador ( ver ultimo punto de instalación y arranque ) ir a gestión > gestión de recursos y añadir los recursos al sistema recordando poner las IPs de los enchufes obtenidas en el paso anterior.
Una vez añadidos los recursos con las IPs correspondientes a los enchufes, ActiveSpace está listo para usarse.

Prepara n raspberry pi con el SO Raspbian, conectalos a la misma red wi-fi, situalos junto a los recusos y ejecuta el cliente disponible en la carpeta lib de este proyecto. (Nota: hay que configurar cada cliente en cada raspberry pi con la ip del recurso al que pertenece).

Ahora ya puedes conectar el lector RFID en la raspberry pi.
